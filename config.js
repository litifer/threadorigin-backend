var MAIN_URL = /* 'http://13.232.65.165/threadorigin' */ 'https://dev.threadorigin.com/threadorigin';
var BASE_URL = /*'http://52.66.118.207:8000/api/v1/'*/ /*'http://localhost:8000/api/v1/'*/ 'https://api.threadorigin.com/api/v1/';


var config = require('konfig')({ path: './config' }).app
// var MAIN_URL = serverconfig.MAIN_URL
// var BASE_URL = config.BASE_URL
// debugger
var api_constants = {

    BASE_URL: config.BASE_URL,
    DATABASE_URL: config.MONGO_URL,
    //magento login
    ADMIN_USERNAME: config.ADMIN_USERNAME,
    ADMIN_PASSWORD: config.ADMIN_PASSWORD,

    //magento admin access token
    ACCESS_TOKEN: config.ACCESS_TOKEN,

    //instagram access token
    INSTAGRAM_ACCESS_TOKEN: config.INSTAGRAM_ACCESS_TOKEN,

    //url for content
    MEDIA_BASE_URL: config.MEDIA_BASE_URL,
    


    // DATABASE_URL: 'mongodb://litifer_ankit:6pathsofPain@ds223268.mlab.com:23268/thread-origin-db',
    // //magento login
    // ADMIN_USERNAME: 'admin_new',
    // ADMIN_PASSWORD: 'Qwerty123',

    // //magento admin access token
    // ACCESS_TOKEN: "39sqyoc46rqy45k9s9r2qfxi2h590fcd",

    // //instagram access token
    // INSTAGRAM_ACCESS_TOKEN: '8006095402.8a540c5.df7cf68e50b241c39ea3dd6463452527',

    // //url for content
    // MEDIA_BASE_URL: MAIN_URL + '/pub/media/catalog/product/',

    //API's
    EXCLUSIVE_URL: MAIN_URL + "/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=4&searchCriteria[filter_groups][1][filters][0][field]=type_id&searchCriteria[filter_groups][1][filters][0][value]=configurable",
    ADMIN_URL: MAIN_URL + '/rest/V1/integration/admin/token',
    PRODUCT_INFO: MAIN_URL + '/rest/V1/products/',
    CATEGORIES_URL: MAIN_URL + '/rest/V1/categories',
    HOME_CAMP_1: MAIN_URL + "/rest/V1/products/hcamp1",
    HOME_CAMP_2: MAIN_URL + "/rest/V1/products/hcamp2",
    HOME_CAMP_3: MAIN_URL + "/rest/V1/products/hcamp3",
    CATEGORY_CAMP_1: MAIN_URL + "/rest/V1/products/ccamp1",
    SEARCH_PRODUCT: MAIN_URL + "/rest/V1/products?searchCriteria[filter_groups][1][filters][0][field]=category_id&searchCriteria[filter_groups][1][filters][0][value]=",
    CONFIGURABLE_PRODUCTS: "&searchCriteria[filter_groups][1][filters][0][field]=type_id&searchCriteria[filter_groups][1][filters][0][value]=configurable",
    ADD_ACCOUNT: MAIN_URL + "/rest/V1/customers",
    GET_LOOKBOOK_CATEGORY_IDS: MAIN_URL + "/rest/V1/categories",
    GET_DESIGNER_DETAILS: MAIN_URL + "/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=name&searchCriteria[filterGroups][0][filters][0][condition_type]=eq&searchCriteria[filterGroups][0][filters][0][value]=",
    GET_IMAGES: BASE_URL + "/media/images",
    GET_LOOKBOOK_DESC: MAIN_URL + "/rest/V1/products",
    GET_CAMPAIGN_LOOKS: MAIN_URL + "/rest/V1/categories/:campaignId/products",
    GET_ACCOUNT: MAIN_URL + "/rest/V1/customers/me",
    AUTHENTICATE_USER: MAIN_URL + "/rest/V1/integration/customer/token",

    // guest cart routes
    GUEST_USER_CART: MAIN_URL + "/rest/V1/guest-carts/",
    ADD_TO_CART: MAIN_URL + "/rest/default/V1/guest-carts/",

    // user cart items
    GET_USER_CART: MAIN_URL + '/rest/V1/carts/mine',
    UPDATE_ITEM_IN_USER_CART: MAIN_URL + '/rest/V1/carts/mine/items/',
    GET_USER_CART_PAYMENT_OPTIONS: MAIN_URL + '/rest/V1/carts/mine/payment-information',
    USER_CART: MAIN_URL + "/rest/V1/carts/",
    ADD_ITEM_TO_USER_CART: MAIN_URL + "/rest/V1/carts/mine/items",
    GET_USER_CART_ID: MAIN_URL + "/rest/V1/carts/mine",
    DELETE_CART_ITEM: MAIN_URL + "/rest/V1/carts/mine/items/",
    PRODUCT_VARIANTS: MAIN_URL + "/rest/V1/configurable-products/",
    PRODUCT_COLORS: MAIN_URL + "/rest/V1/products/attributes?searchCriteria[filter_groups][0][filters][0][field]=attribute_code&searchCriteria[filter_groups][0][filters][0][value]=color",
    PRODUCT_SIZES: MAIN_URL + "/rest/V1/products/attributes?searchCriteria[filter_groups][0][filters][0][field]=attribute_code&searchCriteria[filter_groups][0][filters][0][value]=size",
    PRODUCT_SORT_COLOR: MAIN_URL + "/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=color&searchCriteria[filterGroups][0][filters][0][value]=",
    PRODUCT_SORT_SIZE: MAIN_URL + "/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=size&searchCriteria[filterGroups][0][filters][0][value]=",
    ADD_COUPON_CODE: MAIN_URL + "/rest/V1/carts/mine/coupons/",
    REMOVE_COUPON_CODE: MAIN_URL + "/rest/V1/carts/mine/coupons",
    ORDER_DETAILS: MAIN_URL + "/rest/V1/orders?searchCriteria[filter_groups][0][filters][0][field]=customer_id&searchCriteria[filter_groups][0][filters][0][value]=",
    ADD_WISHLIST: MAIN_URL + "/rest/V1/wishlist/add/",
    ORDER_CART_PRODUCTS: MAIN_URL + "/rest/V1/carts/mine/payment-information",
    ALL_ORDERS: MAIN_URL + '/rest/V1/orders?searchCriteria[filter_groups][0][filters][0][field]=customer_email&searchCriteria[filter_groups][0][filters][0][value]=',
    GET_WISHLIST: MAIN_URL + "/rest/V1/wishlist/items",
    UPDATE_WISHLIST: MAIN_URL + "/rest/V1/wishlist/delete/",
    NEWSLETTER_SUBSCRIPTION: MAIN_URL + "/rest/V1/customers/me",
    EXCHANGE_RATES: "https://openexchangerates.org/api/latest.json?app_id=4476b968150942948ae41d58196196ce&symbols=INR,SGD,CNY,AED,GBP,EUR,AUD,CAD,USD",
    GEO_IP: "http://ip-api.com/json/",
    SEARCH_1: MAIN_URL + "/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=name&searchCriteria[filter_groups][0][filters][0][value]=%",
    SEARCH_2: '%&searchCriteria[filter_groups][0][filters][0][condition_type]=like',
    CAT_PRODUCT_COLORS: MAIN_URL + "/rest/V1/products/attributes?searchCriteria[filter_groups][0][filters][0][field]=attribute_code&searchCriteria[filter_groups][0][filters][0][value]=color&fields=items[options]",
    CAT_PRODUCT_SIZES: MAIN_URL + "/rest/V1/products/attributes?searchCriteria[filter_groups][0][filters][0][field]=attribute_code&searchCriteria[filter_groups][0][filters][0][value]=size&fields=items[options]",
    GUEST_CART: MAIN_URL + "/rest/V1/guest-carts",
    GUEST_ADD_ITEM: MAIN_URL + "/rest/V1/guest-carts/",
    CART_TOTAL: MAIN_URL + "/rest/V1/carts/mine/totals?fields=grand_total,items_qty,items[item_id,name,qty,base_row_total],discount_amount",
    UPDATE_PASSWORD: MAIN_URL + "/rest/V1/customers/me/password",
    UPDATE_ACCOUNT: MAIN_URL + "/rest/V1/customers/me",
    DELETE_ADDRESS: MAIN_URL + "/rest/V1/addresses/",
    RESET_PASSWORD: MAIN_URL + "/rest/V1/customers/password",
    COUNTRIES: '[{"name":"Afghanistan","alpha2Code":"AF"},{"name":"Åland Islands","alpha2Code":"AX"},{"name":"Albania","alpha2Code":"AL"},{"name":"Algeria","alpha2Code":"DZ"},{"name":"American Samoa","alpha2Code":"AS"},{"name":"Andorra","alpha2Code":"AD"},{"name":"Angola","alpha2Code":"AO"},{"name":"Anguilla","alpha2Code":"AI"},{"name":"Antarctica","alpha2Code":"AQ"},{"name":"Antigua and Barbuda","alpha2Code":"AG"},{"name":"Argentina","alpha2Code":"AR"},{"name":"Armenia","alpha2Code":"AM"},{"name":"Aruba","alpha2Code":"AW"},{"name":"Australia","alpha2Code":"AU"},{"name":"Austria","alpha2Code":"AT"},{"name":"Azerbaijan","alpha2Code":"AZ"},{"name":"Bahamas","alpha2Code":"BS"},{"name":"Bahrain","alpha2Code":"BH"},{"name":"Bangladesh","alpha2Code":"BD"},{"name":"Barbados","alpha2Code":"BB"},{"name":"Belarus","alpha2Code":"BY"},{"name":"Belgium","alpha2Code":"BE"},{"name":"Belize","alpha2Code":"BZ"},{"name":"Benin","alpha2Code":"BJ"},{"name":"Bermuda","alpha2Code":"BM"},{"name":"Bhutan","alpha2Code":"BT"},{"name":"Bolivia (Plurinational State of)","alpha2Code":"BO"},{"name":"Bonaire, Sint Eustatius and Saba","alpha2Code":"BQ"},{"name":"Bosnia and Herzegovina","alpha2Code":"BA"},{"name":"Botswana","alpha2Code":"BW"},{"name":"Bouvet Island","alpha2Code":"BV"},{"name":"Brazil","alpha2Code":"BR"},{"name":"British Indian Ocean Territory","alpha2Code":"IO"},{"name":"United States Minor Outlying Islands","alpha2Code":"UM"},{"name":"Virgin Islands (British)","alpha2Code":"VG"},{"name":"Virgin Islands (U.S.)","alpha2Code":"VI"},{"name":"Brunei Darussalam","alpha2Code":"BN"},{"name":"Bulgaria","alpha2Code":"BG"},{"name":"Burkina Faso","alpha2Code":"BF"},{"name":"Burundi","alpha2Code":"BI"},{"name":"Cambodia","alpha2Code":"KH"},{"name":"Cameroon","alpha2Code":"CM"},{"name":"Canada","alpha2Code":"CA"},{"name":"Cabo Verde","alpha2Code":"CV"},{"name":"Cayman Islands","alpha2Code":"KY"},{"name":"Central African Republic","alpha2Code":"CF"},{"name":"Chad","alpha2Code":"TD"},{"name":"Chile","alpha2Code":"CL"},{"name":"China","alpha2Code":"CN"},{"name":"Christmas Island","alpha2Code":"CX"},{"name":"Cocos (Keeling) Islands","alpha2Code":"CC"},{"name":"Colombia","alpha2Code":"CO"},{"name":"Comoros","alpha2Code":"KM"},{"name":"Congo","alpha2Code":"CG"},{"name":"Congo (Democratic Republic of the)","alpha2Code":"CD"},{"name":"Cook Islands","alpha2Code":"CK"},{"name":"Costa Rica","alpha2Code":"CR"},{"name":"Croatia","alpha2Code":"HR"},{"name":"Cuba","alpha2Code":"CU"},{"name":"Curaçao","alpha2Code":"CW"},{"name":"Cyprus","alpha2Code":"CY"},{"name":"Czech Republic","alpha2Code":"CZ"},{"name":"Denmark","alpha2Code":"DK"},{"name":"Djibouti","alpha2Code":"DJ"},{"name":"Dominica","alpha2Code":"DM"},{"name":"Dominican Republic","alpha2Code":"DO"},{"name":"Ecuador","alpha2Code":"EC"},{"name":"Egypt","alpha2Code":"EG"},{"name":"El Salvador","alpha2Code":"SV"},{"name":"Equatorial Guinea","alpha2Code":"GQ"},{"name":"Eritrea","alpha2Code":"ER"},{"name":"Estonia","alpha2Code":"EE"},{"name":"Ethiopia","alpha2Code":"ET"},{"name":"Falkland Islands (Malvinas)","alpha2Code":"FK"},{"name":"Faroe Islands","alpha2Code":"FO"},{"name":"Fiji","alpha2Code":"FJ"},{"name":"Finland","alpha2Code":"FI"},{"name":"France","alpha2Code":"FR"},{"name":"French Guiana","alpha2Code":"GF"},{"name":"French Polynesia","alpha2Code":"PF"},{"name":"French Southern Territories","alpha2Code":"TF"},{"name":"Gabon","alpha2Code":"GA"},{"name":"Gambia","alpha2Code":"GM"},{"name":"Georgia","alpha2Code":"GE"},{"name":"Germany","alpha2Code":"DE"},{"name":"Ghana","alpha2Code":"GH"},{"name":"Gibraltar","alpha2Code":"GI"},{"name":"Greece","alpha2Code":"GR"},{"name":"Greenland","alpha2Code":"GL"},{"name":"Grenada","alpha2Code":"GD"},{"name":"Guadeloupe","alpha2Code":"GP"},{"name":"Guam","alpha2Code":"GU"},{"name":"Guatemala","alpha2Code":"GT"},{"name":"Guernsey","alpha2Code":"GG"},{"name":"Guinea","alpha2Code":"GN"},{"name":"Guinea-Bissau","alpha2Code":"GW"},{"name":"Guyana","alpha2Code":"GY"},{"name":"Haiti","alpha2Code":"HT"},{"name":"Heard Island and McDonald Islands","alpha2Code":"HM"},{"name":"Holy See","alpha2Code":"VA"},{"name":"Honduras","alpha2Code":"HN"},{"name":"Hong Kong","alpha2Code":"HK"},{"name":"Hungary","alpha2Code":"HU"},{"name":"Iceland","alpha2Code":"IS"},{"name":"India","alpha2Code":"IN"},{"name":"Indonesia","alpha2Code":"ID"},{"name":"Côte dIvoire","alpha2Code":"CI"},{"name":"Iran (Islamic Republic of)","alpha2Code":"IR"},{"name":"Iraq","alpha2Code":"IQ"},{"name":"Ireland","alpha2Code":"IE"},{"name":"Isle of Man","alpha2Code":"IM"},{"name":"Israel","alpha2Code":"IL"},{"name":"Italy","alpha2Code":"IT"},{"name":"Jamaica","alpha2Code":"JM"},{"name":"Japan","alpha2Code":"JP"},{"name":"Jersey","alpha2Code":"JE"},{"name":"Jordan","alpha2Code":"JO"},{"name":"Kazakhstan","alpha2Code":"KZ"},{"name":"Kenya","alpha2Code":"KE"},{"name":"Kiribati","alpha2Code":"KI"},{"name":"Kuwait","alpha2Code":"KW"},{"name":"Kyrgyzstan","alpha2Code":"KG"},{"name":"Lao Peoples Democratic Republic","alpha2Code":"LA"},{"name":"Latvia","alpha2Code":"LV"},{"name":"Lebanon","alpha2Code":"LB"},{"name":"Lesotho","alpha2Code":"LS"},{"name":"Liberia","alpha2Code":"LR"},{"name":"Libya","alpha2Code":"LY"},{"name":"Liechtenstein","alpha2Code":"LI"},{"name":"Lithuania","alpha2Code":"LT"},{"name":"Luxembourg","alpha2Code":"LU"},{"name":"Macao","alpha2Code":"MO"},{"name":"Macedonia (the former Yugoslav Republic of)","alpha2Code":"MK"},{"name":"Madagascar","alpha2Code":"MG"},{"name":"Malawi","alpha2Code":"MW"},{"name":"Malaysia","alpha2Code":"MY"},{"name":"Maldives","alpha2Code":"MV"},{"name":"Mali","alpha2Code":"ML"},{"name":"Malta","alpha2Code":"MT"},{"name":"Marshall Islands","alpha2Code":"MH"},{"name":"Martinique","alpha2Code":"MQ"},{"name":"Mauritania","alpha2Code":"MR"},{"name":"Mauritius","alpha2Code":"MU"},{"name":"Mayotte","alpha2Code":"YT"},{"name":"Mexico","alpha2Code":"MX"},{"name":"Micronesia (Federated States of)","alpha2Code":"FM"},{"name":"Moldova (Republic of)","alpha2Code":"MD"},{"name":"Monaco","alpha2Code":"MC"},{"name":"Mongolia","alpha2Code":"MN"},{"name":"Montenegro","alpha2Code":"ME"},{"name":"Montserrat","alpha2Code":"MS"},{"name":"Morocco","alpha2Code":"MA"},{"name":"Mozambique","alpha2Code":"MZ"},{"name":"Myanmar","alpha2Code":"MM"},{"name":"Namibia","alpha2Code":"NA"},{"name":"Nauru","alpha2Code":"NR"},{"name":"Nepal","alpha2Code":"NP"},{"name":"Netherlands","alpha2Code":"NL"},{"name":"New Caledonia","alpha2Code":"NC"},{"name":"New Zealand","alpha2Code":"NZ"},{"name":"Nicaragua","alpha2Code":"NI"},{"name":"Niger","alpha2Code":"NE"},{"name":"Nigeria","alpha2Code":"NG"},{"name":"Niue","alpha2Code":"NU"},{"name":"Norfolk Island","alpha2Code":"NF"},{"name":"Korea (Democratic Peoples Republic of)","alpha2Code":"KP"},{"name":"Northern Mariana Islands","alpha2Code":"MP"},{"name":"Norway","alpha2Code":"NO"},{"name":"Oman","alpha2Code":"OM"},{"name":"Pakistan","alpha2Code":"PK"},{"name":"Palau","alpha2Code":"PW"},{"name":"Palestine, State of","alpha2Code":"PS"},{"name":"Panama","alpha2Code":"PA"},{"name":"Papua New Guinea","alpha2Code":"PG"},{"name":"Paraguay","alpha2Code":"PY"},{"name":"Peru","alpha2Code":"PE"},{"name":"Philippines","alpha2Code":"PH"},{"name":"Pitcairn","alpha2Code":"PN"},{"name":"Poland","alpha2Code":"PL"},{"name":"Portugal","alpha2Code":"PT"},{"name":"Puerto Rico","alpha2Code":"PR"},{"name":"Qatar","alpha2Code":"QA"},{"name":"Republic of Kosovo","alpha2Code":"XK"},{"name":"Réunion","alpha2Code":"RE"},{"name":"Romania","alpha2Code":"RO"},{"name":"Russian Federation","alpha2Code":"RU"},{"name":"Rwanda","alpha2Code":"RW"},{"name":"Saint Barthélemy","alpha2Code":"BL"},{"name":"Saint Helena, Ascension and Tristan da Cunha","alpha2Code":"SH"},{"name":"Saint Kitts and Nevis","alpha2Code":"KN"},{"name":"Saint Lucia","alpha2Code":"LC"},{"name":"Saint Martin (French part)","alpha2Code":"MF"},{"name":"Saint Pierre and Miquelon","alpha2Code":"PM"},{"name":"Saint Vincent and the Grenadines","alpha2Code":"VC"},{"name":"Samoa","alpha2Code":"WS"},{"name":"San Marino","alpha2Code":"SM"},{"name":"Sao Tome and Principe","alpha2Code":"ST"},{"name":"Saudi Arabia","alpha2Code":"SA"},{"name":"Senegal","alpha2Code":"SN"},{"name":"Serbia","alpha2Code":"RS"},{"name":"Seychelles","alpha2Code":"SC"},{"name":"Sierra Leone","alpha2Code":"SL"},{"name":"Singapore","alpha2Code":"SG"},{"name":"Sint Maarten (Dutch part)","alpha2Code":"SX"},{"name":"Slovakia","alpha2Code":"SK"},{"name":"Slovenia","alpha2Code":"SI"},{"name":"Solomon Islands","alpha2Code":"SB"},{"name":"Somalia","alpha2Code":"SO"},{"name":"South Africa","alpha2Code":"ZA"},{"name":"South Georgia and the South Sandwich Islands","alpha2Code":"GS"},{"name":"Korea (Republic of)","alpha2Code":"KR"},{"name":"South Sudan","alpha2Code":"SS"},{"name":"Spain","alpha2Code":"ES"},{"name":"Sri Lanka","alpha2Code":"LK"},{"name":"Sudan","alpha2Code":"SD"},{"name":"Suriname","alpha2Code":"SR"},{"name":"Svalbard and Jan Mayen","alpha2Code":"SJ"},{"name":"Swaziland","alpha2Code":"SZ"},{"name":"Sweden","alpha2Code":"SE"},{"name":"Switzerland","alpha2Code":"CH"},{"name":"Syrian Arab Republic","alpha2Code":"SY"},{"name":"Taiwan","alpha2Code":"TW"},{"name":"Tajikistan","alpha2Code":"TJ"},{"name":"Tanzania, United Republic of","alpha2Code":"TZ"},{"name":"Thailand","alpha2Code":"TH"},{"name":"Timor-Leste","alpha2Code":"TL"},{"name":"Togo","alpha2Code":"TG"},{"name":"Tokelau","alpha2Code":"TK"},{"name":"Tonga","alpha2Code":"TO"},{"name":"Trinidad and Tobago","alpha2Code":"TT"},{"name":"Tunisia","alpha2Code":"TN"},{"name":"Turkey","alpha2Code":"TR"},{"name":"Turkmenistan","alpha2Code":"TM"},{"name":"Turks and Caicos Islands","alpha2Code":"TC"},{"name":"Tuvalu","alpha2Code":"TV"},{"name":"Uganda","alpha2Code":"UG"},{"name":"Ukraine","alpha2Code":"UA"},{"name":"United Arab Emirates","alpha2Code":"AE"},{"name":"United Kingdom of Great Britain and Northern Ireland","alpha2Code":"GB"},{"name":"United States of America","alpha2Code":"US"},{"name":"Uruguay","alpha2Code":"UY"},' +
        '{"name":"Uzbekistan","alpha2Code":"UZ"},{"name":"Vanuatu","alpha2Code":"VU"},{"name":"Venezuela (Bolivarian Republic of)","alpha2Code":"VE"},{"name":"Viet Nam","alpha2Code":"VN"},{"name":"Wallis and Futuna","alpha2Code":"WF"},{"name":"Western Sahara","alpha2Code":"EH"},{"name":"Yemen","alpha2Code":"YE"},{"name":"Zambia","alpha2Code":"ZM"},{"name":"Zimbabwe","alpha2Code":"ZW"}]',
    SHIPPING_METHOD: MAIN_URL + '/rest/V1/carts/mine/shipping-information',
    USER_BILLING_METHOD: MAIN_URL + '/rest/V1/carts/mine/billing-address',
    PAYMENT_METHOD: MAIN_URL + '/rest/V1/carts/mine/payment-information',
    GUEST_PAYMENT_METHOD: MAIN_URL + '/rest/V1/guest-carts',
    GUEST_CART_TOTAL: MAIN_URL + '/rest/V1/guest-carts/',
    GUEST_CART_TOTAL2: "/totals?fields=grand_total,items_qty,items[item_id,name,qty,base_row_total],discount_amount",
    GUEST_SHIPPING_INFO: MAIN_URL + '/rest/V1/guest-carts/',


    // my custom endpoints
    PRODUCT_BY_CATEGORY: MAIN_URL + '/rest/V1/categories/',

    //generate coupon codes
    GENERATE_COUPON_CODE: MAIN_URL + '/rest/V1/coupons/generate',
    GET_MEDIA_URL: MAIN_URL + '/rest/default/V1/products/',
    APPLY_COUPON_CODE: '/coupons/',


    // billing info endpoints
    PAYMENT_INFO: '/payment-information',
    SHIPPING_INFO: '/shipping-information',
    BILLING_ADDRESS: '/billing-address'
}

module.exports = api_constants;



