### ACCOUNT API's


### ADD CUSTOMER ACCOUNT

_POST_  `/api/account`

_INPUT_ 

- `password` must be combination of Upper,lower letters,numbers and special symbol (Minimum of 8 characters)

```
{
	"email" : "test@gmail.com",
	"password" : "Qwerty123",   
	"firstname" : "fname",
	"lastname" : "lname"
}

```

_RESPONSE_      `200 OK`

```
{  
   "id":2,
   "email":"test2@gmail.com",
   "firstname":"fname",
   "lastname":"lname"
}

```

### GET CUSTOMER ACCOUNT 

_GET_ `/api/account`

- `id` is user id
- `user token required`

_RESPONSE_ `200 OK`

```
{  
   "id":2,
   "email":"test2@gmail.com",
   "firstname":"fname",
   "lastname":"lname",
   "address" : []
}

```

### CUSTOMER LOGIN VALIDATION / GET USER TOKEN

_POST_  `/api/account/validate`

_INPUT_ 

```
{
   "email":"test2@gmail.com",
   "password" : "password"
}

```

_RESPONSE_ `200 OK`

```
{
    "success": "dgngjdn6lpidxd1xkpfwe7wt85ia19c2"   //user token
}

```
- if failed returns `{"success" : "false"}`


### UPDATE PASSWORD

_POST_  `/api/account/password`

- `user token` required

_INPUT_

```
{
    "current" : "current_password",
    "new" : "new_password"
}
```

_RESPONSE_ `200 OK`

```
true

```

### UPDATE ACCOUNT INFO

_POST_ `/api/account/update`

-`user token` required

_INPUT_ 

```
{
    "email": req.body.email,
    "firstname": req.body.firstname,
    "lastname": req.body.lastname,

    //These are optional (When customer wants to update password //also)
    "change_password" : true,
    "current" : "current_password",
    "new_pass" : "new_pass" // Combination of Capital,Small,                                //Number and Special symbol (Min. 8 chars)
}

```

_RESPONSE_ `200 OK`

```
{
    "Password_Change_Status": [
        "false"
    ],
    "Account_Info": [
        {
            "id": 1,
            "email": "test@gmail.com",
            "firstname": "Test",
            "lastname": "Account",
            "addresses": [
                {
                    "id": 5,
                    "customer_id": 1,
                    "region": {
                        "region_code": "string",
                        "region": "string",
                        "region_id": 0
                    },
                    "region_id": 0,
                    "country_id": "INDIA",
                    "street": [
                        "GURUGRAM"
                    ],
                    "telephone": "123456789",
                    "postcode": "123456",
                    "city": "Delhi",
                    "firstname": "string",
                    "lastname": "string"
                }
            ]
        }
    ]
}

```


### GET CUSTOMER ADDRESSES

_GET_ `/api/account/addresses`

-`user token` required

_RESPONSE_ `200 OK`

```
{
    "addresses": [
                {
                    "id": 5,
                    "customer_id": 1,
                    "region": {
                        "region_code": "string",
                        "region": "string",
                        "region_id": 0
                    },
                    "region_id": 0,
                    "country_id": "INDIA",
                    "street": [
                        "GURUGRAM"
                    ],
                    "telephone": "123456789",
                    "postcode": "123456",
                    "city": "Delhi",
                    "firstname": "string",
                    "lastname": "string"
                },..
            ]
}

```


### CUSTOMER ACCCOUT DASHBOARD

_GET_ `/api/account/dashboard`

-`user token` required

_RESPONSE_ `200 OK`

```
{
    "subcription_status": [
        false
    ],
    "info": [
        {
            "email": "test@gmail.com",
            "firstname": "Test",
            "lastname": "Account",
            "addresses": [
                {
                    "id": 5,
                    "customer_id": 1,
                    "region": {
                        "region_code": "string",
                        "region": "string",
                        "region_id": 0
                    },
                    "region_id": 0,
                    "country_id": "INDIA",
                    "street": [
                        "GURUGRAM"
                    ],
                    "telephone": "123456789",
                    "postcode": "123456",
                    "city": "Delhi",
                    "firstname": "string",
                    "lastname": "string"
                }
            ]
        }
    ]
}

```

### DELETE CUSTOMER ADDRESS

_GET_ `/api/account/addresses/:address`

- `:address` is address id

- `user token` required

_RESPONSE_ `200 OK`

```
true

```

### UPDATE CUSTOMER ADDRESS

_POST_ `/api/account/update/address`

- `user token` required

_INPUT_

```
{
    "id" : "address_id",        //address that needs to be updated
    "telephone": "123456789",
    "postcode": "123456",
    "city": "Delhi",
    "firstname": "string",
    "lastname": "string",
    "street" : "string",
    "country" : "string",
    "default_shipping" : true, //Optional
    "default_billing" : true //Optional
}

```

_RESPONSE_ `200 OK`

```
{
    "id": 1,
    "default_billing": "5",
    "default_shipping": "5",
    "email": "test@gmail.com",
    "firstname": "Test",
    "lastname": "Account",
    "addresses": [
        {
            "id": 5,
            "customer_id": 1,
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": "string",
            "street": [
                "string"
            ],
            "telephone": "123456789",
            "postcode": "123456",
            "city": "Delhi",
            "firstname": "string",
            "lastname": "string",
            "default_shipping": true,
            "default_billing": true
        }
    ]
}


```

### RESET PASSWORD EMAIL

_POST_ `/api/account/reset`

_INPUT_

```
{
    "email" : "email@gmail.com"
}
```

_REPSONSE_ `200 OK`

```
true

```

### ADD NEW ADDRESS

_POST_  `/api/account/address/new`

-`user token` required

_INPUT_ 

```
    "telephone": "123456789",
    "postcode": "123456",
    "city": "Delhi",
    "firstname": "string",
    "lastname": "string",
    "street" : "string",
    "country" : "string"

```

_RESPONSE_ `200 OK`

```
{
    "id": 1,
    "default_billing": "5",
    "default_shipping": "5",
    "email": "test@gmail.com",
    "firstname": "Test",
    "lastname": "Account",
    "addresses": [
        {
            "id": 5,
            "customer_id": 1,
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": "string",
            "street": [
                "string"
            ],
            "telephone": "123456789",
            "postcode": "123456",
            "city": "Delhi",
            "firstname": "string",
            "lastname": "string"
        }
    ]
}

```

### ADD NEW DEFAULT ADDRESS

_POST_ `/api/account/address/default`

-`user token` is required

_INPUT_ 

```
    "telephone": "123456789",
    "postcode": "123456",
    "city": "Delhi",
    "firstname": "string",
    "lastname": "string",
    "street" : "string",
    "country" : "string"

```

_RESPONSE_ `200 OK`

```
{
    "id": 1,
    "default_billing": "5",
    "default_shipping": "5",
    "email": "test@gmail.com",
    "firstname": "Test",
    "lastname": "Account",
    "addresses": [
        {
            "id": 5,
            "customer_id": 1,
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": "string",
            "street": [
                "string"
            ],
            "telephone": "123456789",
            "postcode": "123456",
            "city": "Delhi",
            "firstname": "string",
            "lastname": "string",
            "default_shipping": true,
            "default_billing": true
        }
    ]
}

```

### CHANGE DEFAULT ADDRESS

_GET_ `/api/account/address/default/:id`

- `:id` is address id to make it default

_RESPONSE_ `200 OK`

```
{
    "id": 1,
    "default_billing": "5",
    "default_shipping": "5",
    "email": "test@gmail.com",
    "firstname": "Test",
    "lastname": "Account",
    "addresses": [
        {
            "id": 5,
            "customer_id": 1,
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": "string",
            "street": [
                "string"
            ],
            "telephone": "123456789",
            "postcode": "123456",
            "city": "Delhi",
            "firstname": "string",
            "lastname": "string",
            "default_shipping": true,
            "default_billing": true
        }
    ]
}

```

