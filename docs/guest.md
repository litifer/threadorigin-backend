### GUEST API's

### Generate Guest Cart

_GET_  `/api/guest`

_RESPONSE_  `200 OK`

```
{
    "cart_id": "b0cbbd43a8a28b7d2b2484fa51ecf946"
}

```

### Add item to cart

_POST_ `/api/guest/cart/:id`

- `id` is cart id from generated guest cart

_INPUT_

```
{
    "sku" : "1",
    "qty" : 2,
    "color_id" : 12,
    "color_value" : 2,
    "size_id" : 12,
    "size_value" : 2

}

```

_RESPONE_ `200 OK`

```
{ "message" : 'Items added!'}

```


### GET CART ITEMS


_GET_ `/api/guest/items/:id`

- `id` is guest cart id 

_RESPONSE_ `200 OK`

```
{
    "Product_Info": [
        {
            "name": "Sasha BodyCon  Dress",
            "qty": 2,
            "item_id": 19,
            "price": 200,
            "product_option": {
                "extension_attributes": {
                    "configurable_item_options": [
                        {
                            "option_id": "93",
                            "option_value": 4
                        },
                        {
                            "option_id": "135",
                            "option_value": 5
                        }
                    ]
                }
            }
        },
        {
            "grand_total": {
                "grand_total": 200,
                "discount_amount": 0,
                "items_qty": 2,
                "items": [
                    {
                        "item_id": 19,
                        "qty": 2,
                        "base_row_total": 200,
                        "name": "Sasha BodyCon  Dress"
                    }
                ]
            }
        }
    ]
}

```


### UPDATE CART ITEM

_POST_  `/api/guest/update`

_INPUT_ 

```
{
	"item" : "19",  //item id
    "qty" : 2,
    "cart" : "708653bcc58ed5c7151522e6ff1d7cd2" //guest cart id
}

```
_RESPONSE_ `200 OK`

```
{
    "Product_Info": [
        {
            "name": "Sasha BodyCon  Dress",
            "qty": 2,
            "item_id": 19,
            "price": 200,
            "product_option": {
                "extension_attributes": {
                    "configurable_item_options": [
                        {
                            "option_id": "93",
                            "option_value": 4
                        },
                        {
                            "option_id": "135",
                            "option_value": 5
                        }
                    ]
                }
            }
        },
        {
            "grand_total": {
                "grand_total": 200,
                "discount_amount": 0,
                "items_qty": 2,
                "items": [
                    {
                        "item_id": 19,
                        "qty": 2,
                        "base_row_total": 200,
                        "name": "Sasha BodyCon  Dress"
                    }
                ]
            }
        }
    ]
}

```

### DELETE CART ITEM

_POST_ `/api/guest/delete`

_INPUT_ 

```
{
	"item" : "20",  //item id
    "cart" : "708653bcc58ed5c7151522e6ff1d7cd2" //guest cart id
}

```
_RESPONSE_ `200 OK`

```
{
    "Product_Info": [
        {
            "name": "Sasha BodyCon  Dress",
            "qty": 2,
            "item_id": 19,
            "price": 200,
            "product_option": {
                "extension_attributes": {
                    "configurable_item_options": [
                        {
                            "option_id": "93",
                            "option_value": 4
                        },
                        {
                            "option_id": "135",
                            "option_value": 5
                        }
                    ]
                }
            }
        },
        {
            "grand_total": {
                "grand_total": 200,
                "discount_amount": 0,
                "items_qty": 2,
                "items": [
                    {
                        "item_id": 19,
                        "qty": 2,
                        "base_row_total": 200,
                        "name": "Sasha BodyCon  Dress"
                    }
                ]
            }
        }
    ]
}

```


### CART MERGE

_POST_ `/api/guest/merge`

- `user token` required

_INPUT_

```
{
    "guest" : "708653bcc58ed5c7151522e6ff1d7cd2"   //Guest cart id
}

```

_RESPONSE_ `200 OK`

```
{
    "message": "Cart merged!"
}

```

### ADD SHIPPING METHOD

_POST_ `/api/guest/shipping`

_INPUT_ 

```
{
	
	"shipping_address" : {
		
		"country" : "India",
		"postcode" : "123124",
		"city" : "Gurgaon",
		"firstname" : "First",
		"lastname" : "Last",
		"telephone" : "123574",
		"street" : "Udyog vihar phase 2",
		"email" : "jintuisbusy@gmail.com"
			
	},
	
	"billing_address" : {
		
	    "country" : "India",
		"postcode" : "123124",
		"city" : "Gurgaon",
		"firstname" : "First",
		"lastname" : "Last",
		"telephone" : "123574",
		"street" : "Udyog vihar phase 2",
		"email" : "jintuisbusy@gmail.com"
		
	},
	
   "guest" : "708653bcc58ed5c7151522e6ff1d7cd2"
}

```

_RESPONSE_

```
{
    "message": "Shipping method saved"
}

```

### ADD PAYMENT METHOD

_POST_ `/api/guest/payment`

_INPUT_ 

```
{
	
	"billing_address" : {
		
	    "country" : "India",
		"postcode" : "123124",
		"city" : "Gurgaon",
		"firstname" : "First",
		"lastname" : "Last",
		"telephone" : "123574",
		"street" : "Udyog vihar phase 2",
		"email" : "jintuisbusy@gmail.com"
		
	},
	
   "guest" : "708653bcc58ed5c7151522e6ff1d7cd2"
}

```

_RESPONSE_

```
{
    "order_id": "3"
}

```