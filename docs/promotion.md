### DISCOUNT API

### APPLY COUPON CODE

_GET_  `/api/coupon/:code`

- `code` is coupon code
- `user token` required
- use `LITIFER` as a coupon code to get Rs.100 off

_RESPONSE_  `200 OK`

```
{
    "discount_status": [
        "true"
    ],
    "info": [
        {
            "grand_total": 2400,
            "discount_amount": -100,
            "items_qty": 1,
            "items": [
                {
                    "item_id": 17,
                    "qty": 1,
                    "base_row_total": 2500,
                    "name": "Sasha BodyCon  Dress"
                }
            ]
        }
    ]
} 

```


