var express = require('express');
var apiRouter = express.Router();
var path = require('path');
var compression = require('compression')
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var Router = require('./router');
var mongoose = require('mongoose');


global.config = require('konfig')({ path: './config' }).app
global.constant = require('./common/api.js')(config.MAIN_URL, config.BASE_URL)
// var config = require('./config');
var app = express();


//Mongoose config
mongoose.connect(config.MONGO_URL).then(
  console.log('\x1b[33m', 'Mongo DB connected...')
);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(compression());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));
app.get('/', function(req, res) {
  res.send('Thread Origin backend working...');
});

apiRouter = Router.mountAPI(apiRouter, config, constant)
app.use('/api/v1', apiRouter);

//catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found ' + req.method + ' ' + req.url + "\n\n");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
