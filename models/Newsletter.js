let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let NewsletterSchema = new Schema({
    email: {
        type: String,
        required: true
    }   
});

module.exports = mongoose.model('Newsletter', NewsletterSchema);

