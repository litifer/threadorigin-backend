const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const wishlist = new Schema({
    userId: {
        type: Number,
        required: true
    },
    wishlist: [
        {
            qty: {
                type: Number,
                required: true
            },
            price: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            sku: {
                type: String,
                required: true
            },
            color_id: {
                type: Number,
                required: true
            },
            color_value: {
                type: Number,
                required: true
            },
            size_id: {
                type: Number,
                required: true
            },
            size_value: {
                type: Number,
                required: true
            }
        }
    ],
    date: {
        type: Date,
         default: Date.now()
    }

});

module.exports = Wishlist = mongoose.model('profile', wishlist);