var authCtrl = require('./controllers/auth');
var homepageCtrl = require('./controllers/homepage');
var productCtrl = require('./controllers/product');
var categoryCtrl = require('./controllers/category');
var accountCtrl = require('./controllers/account');
var userCtrl = require('./controllers/user');
var promotionCtrl = require('./controllers/promotion');
var guestCtrl = require('./controllers/guest');
var optimizeCtrl = require('./controllers/optimized');



exports.mountAPI = (apiRouter, config, constant) => {

/************    Developer Auth API'S     ********/
// get access token
apiRouter.route('/auth/access-token').get(function(req,res){
        authCtrl.getAccessToken(config, constant)
            .then((access_token) => {
                res
                    .status(200)
                    .send({"access_token": access_token})
            })
            .catch((err) => {
                res
                    .status(500)
                    .send(err)

            })
    });

/************     HOMEPAGE API'S     ********/

//Get homepage exclusive products
apiRouter.route('/homepage/exclusive').get(homepageCtrl.getExclusives);

//Get homepage campaign 1
apiRouter.route('/homepage/camp1').get(homepageCtrl.getCamp1);

//Get homepage campaign 2
apiRouter.route('/homepage/camp2').get(homepageCtrl.getCamp2);

//Get homepage campaign 3
apiRouter.route('/homepage/camp3').get(homepageCtrl.getCamp3);

//Get instagram feed
apiRouter.route('/homepage/instafeed').get(homepageCtrl.instaFeed);


/************     MEDIA API'S     ********/

//Media base url
apiRouter.route('/media').get(homepageCtrl.getMediaURL);

//GET media
apiRouter.route('/media/images/:sku').get(productCtrl.getMedia);



/************     PRODUCT API'S     ********/

//Get product details
apiRouter.route('/product/:sku').get(productCtrl.getProductDetails);

//Search products by category
apiRouter.route('/product/all/:category').get(productCtrl.getCategoryProducts);

//Get product variant information
apiRouter.route('/product/variants/:sku').get(productCtrl.getProductVariant);

//Get all color
apiRouter.route('/products/colors').get(productCtrl.getColor);

//Get all sizes
apiRouter.route('/products/sizes').get(productCtrl.getSize);

//Quicksearch Product
apiRouter.route('/product/find/:search').get(productCtrl.searchProduct);

//Currency comversion
apiRouter.route('/products/conversion').post(productCtrl.currenyConvert);

//All details
apiRouter.route('/products/info/:category').get(productCtrl.allProductDetails);



/************     CATEGORY API'S     ********/

//Get categories
apiRouter.route('/categories').get(categoryCtrl.getCategories);

//Get category page campaign
apiRouter.route('/category/campaign').get(categoryCtrl.getCategoryCamp);

//Sort Category products based on color
apiRouter.route('/category/color/:value').get(categoryCtrl.sortCategoryColor);

//Sort Category products based on size
apiRouter.route('/category/size/:value').get(categoryCtrl.sortCategorySize);

apiRouter.route('/category/get-campaign-looks').get(categoryCtrl.getCampaignLooks);
apiRouter.route('/category/get-lookbooks').get(categoryCtrl.getLookbooks);
apiRouter.route('/category/get-designers').get(categoryCtrl.getDesignerData);
apiRouter.route('/category/get-designer-profile/:id').get(categoryCtrl.getProductsAndLookbookData);




/************     ACCOUNT API'S     ********/

//Add an account
apiRouter.route('/account').post(accountCtrl.addAccount);

//Get customer account 
apiRouter.route('/getaccount').post(accountCtrl.getAccount);

//Check user credentials (return customer token)
apiRouter.route('/account/validate').post(accountCtrl.validateAccount);

//Update password
apiRouter.route('/account/password').post(accountCtrl.updatePassword);

//Update Account info
apiRouter.route('/account/update').post(accountCtrl.updateAccount);

//Get All addresses
apiRouter.route('/account/get-addresses').post(accountCtrl.getAddresses);
apiRouter.route('/account/set-address-as-default/:id').post(accountCtrl.setAddressAsDefault);

//Get Account dashboard details
apiRouter.route('/account/dashboard').get(accountCtrl.accountDashboard);

//Delete customer address
apiRouter.route('/account/addresses/:address').get(accountCtrl.deleteAddress);

//Update customer address
apiRouter.route('/account/update/address').post(accountCtrl.updateAddress);

//Reset password
apiRouter.route('/account/reset').post(accountCtrl.resetPasswordEmail);

//Get countries and there codes
apiRouter.route('/countries').get(accountCtrl.getCountries);

//Add new address
apiRouter.route('/account/address/new').post(accountCtrl.addNewAddress);

//Add new default address
apiRouter.route('/account/address/default').post(accountCtrl.addNewDefaultAddress);

//Change default address
apiRouter.route('/account/address/default/:id').post(accountCtrl.changeDefaultAddress);


/**************     USER ACCOUNT RELETED FUNCTIONS API's  ************/

//Create Carts
apiRouter.route('/create/guest-cart/').get(userCtrl.createCart);

//Add item to a cart
apiRouter.route('/add/items/guest-cart/:id').post(userCtrl.addToCart);

//Get cart items
apiRouter.route('/get/user/cart/:id').get(userCtrl.getCart);
// apiRouter.route('/get/user/cart/:id').get(userCtrl.getCart);
// apiRouter.route('/get/user/cart-id').post(userCtrl.getUserCartId);
apiRouter.route('/user/merge-user-and-guest-cart/:guestId').post(userCtrl.mergeUserAndGuestCart);
apiRouter.route('/user/add-item-to-user-cart').post(userCtrl.addItemsToUserCart);
apiRouter.route('/user/get-cart-id').post(userCtrl.getUserCartId);
apiRouter.route('/user/get-user-payment-info').post(userCtrl.getUserPaymentInformation);
apiRouter.route('/user/update-item-in-user-cart').post(userCtrl.updateItemInUserCart);
apiRouter.route('/user/delete-item-from-user-cart').post(userCtrl.deleteItemInUserCart);
apiRouter.route('/user/delete-selected-address').post(accountCtrl.deleteSelectedAddress);
apiRouter.route('/user/edit-selected-address').post(accountCtrl.editSelectedAddress);
apiRouter.route('/user/create-user-wishlist').post(userCtrl.createUserWishlist);
apiRouter.route('/user/add-item-to-user-wishlist').post(userCtrl.addItemToUserWishlist);
apiRouter.route('/user/get-user-wishlist').post(userCtrl.getUserWishlist);
apiRouter.route('/user/delete-item-from-user-wishlist').post(userCtrl.deleteItemFromUserWishlist);
apiRouter.route('/user/add-user-shipping-address').post(userCtrl.addUserShippingAddress);
apiRouter.route('/user/add-user-billing-address').post(userCtrl.addUserBillingAddress);
apiRouter.route('/user/init-order').post(userCtrl.initOrder);
apiRouter.route('/user/payment-info').post(userCtrl.getUserPaymentInformation);
apiRouter.route('/guest/init-order').post(userCtrl.guestInitOrder);


// Get user cart
apiRouter.route('/get/user-cart').post(userCtrl.getUserCart);

//Remove cart item
apiRouter.route('/remove/cart/:cartId/items/:itemId').post(userCtrl.removeCartItem);

//Update Items
apiRouter.route('/update/cart/:cartId/items/:itemId').post(userCtrl.updateCartItem);

//set billing address
apiRouter.route('/add/billing-address/cart/:cartId').post(userCtrl.cartBillingAddress);

// set shipping inofrmation
apiRouter.route('/add/shipping-address/cart/:cartId').post(userCtrl.cartShippingAddress);


//get payment information
apiRouter.route('/get/guest-cart/:id/payment-info').get(userCtrl.getPaymentInformation);









//Get order details via id
apiRouter.route('/user/order/:id').get(userCtrl.getOrderDetails);

//Get order details via email
apiRouter.route('/user/order').post(userCtrl.getOrderDetailsViaEmail);

//Add item to wishlist
apiRouter.route('/user/wishlist/add/:product').get(userCtrl.addToWishlist);

//Get wishlist items
apiRouter.route('/user/wishlist').get(userCtrl.getWishlist);

//Delete wishlist item
apiRouter.route('/user/wishlist/delete/:id').get(userCtrl.updateWishlist);

//Newsletter Subscription
apiRouter.route('/user/newsletter/subscribe').post(userCtrl.subscribeNews);

//Newsletter Un-Subscribe
apiRouter.route('/user/newsletter/unsubscribe').post(userCtrl.unsubscribeNews);

//Newsletter Status
apiRouter.route('/user/newsletter').post(userCtrl.getSubscriptionStatus);

//Update cart item
apiRouter.route('/user/cart/update').post(userCtrl.updateCartItem);

//Add shipping method
apiRouter.route('/user/shipping').post(userCtrl.shippingMethod);

//Add payment method
apiRouter.route('/user/payment').post(userCtrl.paymentMethod);


/****************   PROMOTION API's ***********************/

//Apply coupon to cart 
apiRouter.route('/coupon').post(promotionCtrl.addCouponToCart);

//Remove coupon from cart
apiRouter.route('/delete_coupon').get(promotionCtrl.removeCouponFromCart);

// Generating coupon
apiRouter.route('/generate_coupon').get(promotionCtrl.generateCoupon);

/**************      GUEST CHECKOUT API's    ****************/

//Create empty guest cart
apiRouter.route('/guest').get(guestCtrl.createEmptyCart);

//Add item to cart
apiRouter.route('/guest/cart/:id').post(guestCtrl.addToCart);

//Get cart items
apiRouter.route('/guest/items/:id').get(guestCtrl.getCart);

//Update Cart item
apiRouter.route('/guest/update').post(guestCtrl.updateCartItem);

//Delete cart item
apiRouter.route('/guest/delete').post(guestCtrl.deleteCartItem);

//Merge cart 
apiRouter.route('/guest/merge').post(guestCtrl.mergeCart);

//Add shipping info
apiRouter.route('/guest/shipping').post(guestCtrl.shippingMethod);

//Add payment method
apiRouter.route('/guest/payment').post(guestCtrl.paymentMethod);

/******************************  OPTIMIZED APIs  ******************************/

//get cats
apiRouter.route('/cats').get(optimizeCtrl.optimizedCategories);

return apiRouter;
}

// module.exports = apiRouter(config, constant);