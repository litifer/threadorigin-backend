var request = require('request');
var requestify = require('requestify');
const Wishlist = require('./../models/Wishlist');
var constant = require('../config');
var Newsletter = require('../models/Newsletter');
var promotionCtrl = require('../controllers/promotion');

exports.addToCart = function(req,res){
                    var id = req.params.id;

                        id = id.replace(/\"/g, "")

                        let data = {
                            "cart_item": {
                                "quote_id": id,
                                // "cartId": id,
                                "product_type" : "configurable",
                                "sku": req.body.sku,
                                "qty": parseInt(req.body.qty),
                                "product_option": {
                                    "extension_attributes": {
                                    "configurable_item_options" : [
                                        {
                                            "option_id": parseInt(req.body.color_id), 
                                            "option_value": parseInt(req.body.color_value)
                                        },
                                        {
                                            "option_id": parseInt(req.body.size_id), 
                                            "option_value": parseInt(req.body.size_value)
                                        }
                                     ]
                                   }
                                 }
                                }
                        }
                        console.log("\x1b[36m", "data is...")  
                        console.log(data)
                        console.log("\x1b[34m","id is...")
                        console.log(id)
                        console.log("\x1b[35m","url is...")
                        console.log(constant.ADD_TO_CART + id + '/items')
                        requestify.request(constant.ADD_TO_CART + id + '/items',{
                        method: 'POST',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        },
                        body : data
                    }).then(function(cartResponse) {
                                res.send({cartId: id,product:cartResponse.body});                            
                            })
                    .catch(function(err){
                        console.log(err);
                        res.send(err.body);
                    });
               
};

exports.addItemToUserWishlist = (req, res) => {
    if (req.body.Authorization) {
        let itemData = {
            sku: req.body.sku,
            qty: req.body.qty,
            color_id: req.body.color_id,
            color_value: req.body.color_value,
            size_id: req.body.size_id,
            size_value: req.body.size_value,
            name: req.body.name,
            price: req.body.price
        }
        Wishlist.findOneAndUpdate({userId: req.body.userId}, {$push: {wishlist: itemData}}, {new: true}).then(updatedWishlist => {
            if (updatedWishlist) {
                res.status(200).json(updatedWishlist);
            } else {
                res.status(404).json({errorMessage: "wishlist does not exist"});
            }
        }).catch(err => res.status(400).json({errorMessage: "item could not be added"}));
    } else {
        res.send({errorMessage: "Correct user token is required to create a wishlist."});
    }
}

exports.getUserWishlist = (req, res) => {
    if (req.body.Authorization) {
        Wishlist.findOne({userId: req.body.userId}).then(userWishlist => {
            if (userWishlist) {
                res.status(200).json(userWishlist);
            } else {
                res.status(404).json({errorMessage: "wishlist does not exist"});
            }
        }).catch(err => res.status(400).json({errorMessage: "could not get wishlist"}));
    } else {
        res.send({errorMessage: "Correct user token is required to create a wishlist."});
    }
}

exports.deleteItemFromUserWishlist = (req, res) => {
    if (req.body.Authorization) {
        Wishlist.findOneAndUpdate({userId: req.body.userId}, {$pull: {wishlist: {sku: req.body.sku}}}, {new: true}).then(updatedWishlist => {
            if (updatedWishlist) {
                res.status(200).json(updatedWishlist);
            } else {
                res.status(404).json({errorMessage: "wishlist does not exist"});
            }
        }).catch(err => res.status(400).json({errorMessage: "item could not be removed"}));
    } else {
        res.send({errorMessage: "Correct user token is required."});
    }
}


exports.addUserShippingAddress = (req, res) => {

    console.log(req.body.address, "address incoming from request");

    

    let shippingInfo = {  "addressInformation": {
        "shipping_address": {
         "region": "string",
         "region_id": 0,
         "region_code": "",
         "country_id": req.body.address.country_id,
         "street": req.body.address.street,
      "postcode": req.body.address.postcode,
      "city": req.body.address.city,
      "firstname": req.body.address.firstname,
      "lastname": req.body.address.lastname,
      "email": "jdoe@example.com",
      "telephone": req.body.address.telephone
    },
    "billing_address": {
        "region": "string",
        "region_id": 0,
        "region_code": "",
      "country_id": req.body.address.country_id,
      "street": req.body.address.street,
      "postcode": req.body.address.postcode,
      "city": req.body.address.city,
      "firstname": req.body.address.firstname,
      "lastname": req.body.address.lastname,
      "email": "jdoe@example.com",
      "telephone": req.body.address.telephone
    },
    "shipping_carrier_code": "flatrate",
    "shipping_method_code": "flatrate"
    }
  }
    // let shippingInfo = {
    //     "addressInformation": {
    //        "shippingAddress": {
    //           "city": req.body.address.city,
    //           "country_id": req.body.address.coutnry_id,
    //           "firstname": req.body.address.firstname,
    //           "lastname": req.body.address.lastname,
    //           "postcode": req.body.address.postcode,
    //           "region_code": req.body.address.region.region_code,
    //           "street": req.body.address.street,
    //           "telephone": req.body.address.telephone
    //        },
     
    //       "shippingCarrierCode": "usps",
    //       "shippingMethodCode": "free"
    //     }
    //  }
    console.log(shippingInfo);
    if (req.body.Authorization) {
        let cartId = requestify.request(constant.SHIPPING_METHOD, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: shippingInfo
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}

exports.addUserBillingAddress = (req, res) => {
    let billingInfo = {
        "address": {
        //   "id": 0,
        //   "region": "string",
          "regionId": 0,
          "regionCode": req.body.address.region.region_code,
          "countryId": req.body.address.country_id,
          "street": req.body.address.street,
        //   "company": "string",
          "telephone": req.body.address.telephone,
        //   "fax": "string",
          "postcode": req.body.address.postcode,
          "city": req.body.address.city,
          "firstname": req.body.address.firstname,
          "lastname": req.body.address.lastname,
        //   "middlename": "string",
        //   "prefix": "string",
        //   "suffix": "string",
        //   "vatId": "string",
        //   "customerId": 0,
        //   "email": "string",
        //   "sameAsBilling": 0,
        //   "customerAddressId": 0,
        //   "saveInAddressBook": 0,
        //   "extensionAttributes": {
        //     "giftRegistryId": 0
        //   },
        //   "customAttributes": [
        //     {
        //       "attributeCode": "string",
        //       "value": "string"
        //     }
        //   ]
        },
        "useForShipping": true
      };
    console.log(billingInfo);
    if (req.body.Authorization) {
        let cartId = requestify.request(constant.USER_BILLING_METHOD, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: billingInfo
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}

exports.updateItemInWishlist = (req, res) => {
    if (req.body.Authorization) {
        Wishlist.findOneAndUpdate({userId: req.body.userId}, {$set: {wishlist: {sku: req.body.sku}}}, {new: true}).then(updatedWishlist => {
            if (updatedWishlist) {
                res.status(200).json(updatedWishlist);
            } else {
                res.status(404).json({errorMessage: "wishlist does not exist"});
            }
        }).catch(err => res.status(400).json({errorMessage: "item could not be removed"}));
    } else {
        res.send({errorMessage: "Correct user token is required."});
    }
}

exports.createUserWishlist = (req, res) => {
    if (req.body.Authorization) {
        Wishlist.findOne({userId: parseInt(req.body.userId)}).then(userWishlist => {
            if (userWishlist) {
                res.status(200).json({message: "wishlist already exists for this user"});
            } else {
                const newWishlist = new Wishlist({
                    userId: parseInt(req.body.userId)
                });
                newWishlist.save().then(gottenWishlist => {
                    // console.log(gottenWishlist);
                    res.status(200).json(gottenWishlist);
                }).catch(err => res.status(401).json({errorMessage: "wishlist could not be created"}));
            }
        }).catch(err => res.status(401).json({errorMessage: "wishlist could not be created"}));
    } else {
        res.send({errorMessage: "Correct user token is required to create a wishlist."});
    }
}

exports.addItemsToUserCart = async (req, res) => {
    if (req.body.Authorization) {
        let data = {
            "cart_item": {
                "quote_id": req.body.id,
                // "cartId": id,
                "product_type" : "configurable",
                "sku": req.body.sku,
                "qty": parseInt(req.body.qty),
                "product_option": {
                    "extension_attributes": {
                    "configurable_item_options" : [
                        {
                            "option_id": parseInt(req.body.color_id), 
                            "option_value": parseInt(req.body.color_value)
                        },
                        {
                            "option_id": parseInt(req.body.size_id), 
                            "option_value": parseInt(req.body.size_value)
                        }
                     ]
                   }
                 }
                }
        }
        requestify.request(constant.ADD_ITEM_TO_USER_CART, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: data
        }).then(response => {
            res.send(response.body)
        }).catch(err => {
            res.send(err);
        });
    } else {
        res.send({error: "user token is required"});
    }
}

exports.getUserCartId = (req, res) => {
    if (req.body.Authorization) {
        let cartId = requestify.request(constant.GET_USER_CART_ID, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            }
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}


exports.guestInitOrder = (req, res) => {
    let cartId = requestify.request(constant.GUEST_PAYMENT_METHOD + '/' + req.body.guestCartId + '/payment-information', {
        method: 'POST',
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        },
        body: req.body.paymentData
    }).then(response => {
        res.send(response.body);
    }).catch(err => {
        res.send(err);
    });
}

exports.initOrder = (req, res) => {
    if (req.body.Authorization) {
        let cartId = requestify.request(constant.ORDER_CART_PRODUCTS, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: req.body.paymentData
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}

exports.deleteItemInUserCart = (req, res) => {
    if (req.body.Authorization) {
        let data = {
            "cartItem": {
              "item_id": req.body.itemId,
              "quote_id": req.body.userCartId
            }
          }
        requestify.request(constant.UPDATE_ITEM_IN_USER_CART + req.body.itemId, {
            method: 'DELETE',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: data
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}

exports.updateItemInUserCart = (req, res) => {
    if (req.body.Authorization) {
        let data = {
            "cartItem": {
              "item_id": req.body.itemId, 
              "qty": req.body.qty, 
              "quote_id": req.body.userCartId
            }
          }
        requestify.request(constant.UPDATE_ITEM_IN_USER_CART + req.body.itemId, {
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            },
            body: data
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            res.send(err);
        });
    }
}

exports.mergeUserAndGuestCart = async (req, res) => {
    if (req.body.Authorization) {
        let cartId = requestify.request(constant.GET_USER_CART_ID, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            }
        }).then(responseUserCartId => {
            // res.send(response.body);
            requestify.request(constant.GUEST_USER_CART + req.params.guestId,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            }).then(async responseGuestCart => {
                console.log(responseGuestCart.body);
                // res.send({guestCart: responseGuestCart.body, userCartId: responseUserCartId.body});
                for (let i = 0; i < JSON.parse(responseGuestCart.body).items.length; i++) {
                    const itemObj = JSON.parse(responseGuestCart.body).items[i]
                    console.log(responseUserCartId.body, "user cart id");
                    // let userCartIdInt = ;
                    let responseItemAdded = await requestify.request(constant.ADD_ITEM_TO_USER_CART, {
                        method: 'POST',
                        headers:{
                            'content-type' : 'application/json; charset=utf-8',
                            'Authorization': 'Bearer ' + req.body.Authorization
                        },
                        body: {
                            "cart_item": {
                                "quote_id": JSON.parse(responseUserCartId.body),
                                "sku": itemObj.sku,
                                "qty": itemObj.qty,
                                "name": itemObj.name,
                                "item_id": itemObj.itemId,
                                "price": itemObj.price,
                                "product_type": "configurable",
                              }
                        }
            //             "item_id": 162,
            //             "sku": "3-Black-Large",
            //             "qty": 1,
            //             "name": "Robe Chemise Harvard",
            //             "price": 2000,
            //             "product_type": "configurable",
            //             "quote_id": "152"
                    })/*.then(response => {
                        // res.send(response.body)
                    }).catch(err => {
                        // res.send(err);
                    });*/
                    console.log(responseItemAdded);
                }
                res.send({guestCart: JSON.parse(responseGuestCart.body).items, userCartId: JSON.parse(responseUserCartId.body)});
            }).catch(err => {
                console.log(err);
                res.send(err);
            });     
            // requestify.request(constant.ADD_ITEM_TO_USER_CART, {
            //     method: 'POST',
            //     headers:{
            //         'content-type' : 'application/json; charset=utf-8',
            //         'Authorization': 'Bearer ' + req.body.Authorization
            //     },
            //     body: {
            //         "cart_item": {
            //             "quote_id": req.body.cartId,
            //             "sku": req.body.sku,
            //             "qty": req.body.qty
            //           }
            //     }
            // }).then(response => {
            //     res.send(response.body)
            // }).catch(err => {
            //     res.send(err);
            // });
        }).catch(err => {
            res.send(err);
        });
    } else {
        res.send({error: "user token is required"});
    }
}
exports.addGuestCartItemsToUserCart = async (req, res) => {
    if (req.body.Authorization) {
        requestify.request(constant.GET_USER_CART_ID, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            }
        }).then(response => {
            res.send(response.body)

        }).catch(err => {
            res.send(err);
        });
    } else {
        res.send({error: "user token is required"});
    }
}

exports.createCart = async (req, res) => {
    if(req.body.Authorization)
    {
        requestify.request(constant.USER_CART, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization': 'Bearer ' + req.body.Authorization
            }
        }).then(response => {
            res.send(response.body)
        }).catch(err => {
            res.send(err);
        });
    }
    else
    {
        requestify.request(constant.GUEST_USER_CART, {
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
            }
        }).then(response => {
            res.send(response.body)
        }).catch(err => {
            res.send(err);
        });
    }
    
}

exports.getUserCart = async function(req, res){
    if (req.body.Authorization) {
        requestify.request(constant.GET_USER_CART,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ req.body.Authorization,
                'content-type' : 'application/json; charset=utf-8'
            }
        }).then(response => {
            console.log(response.body);
            res.send(response.body);
        }).catch(err => {
            console.log(err);
            res.send(err);
        });
    } else {
        res.send("User token is required in the body");
    }
};

exports.getCart = async function(req, res){
       requestify.request(constant.GUEST_USER_CART + req.params.id,{
                        method: 'GET',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        }
        }).then(response => {
            console.log(response.body);
            res.send(response);
        }).catch(err => {
            console.log(err);
            res.send(err);
        });
};

async function getItemInfo(response2, token){
    var data_products = await JSON.parse(response2.body);
    let admin_token = await getAdminToken();
    var result = {};
    var data_variants;
    result['Product_Info'] = [];   
    var total = {};
    total['grand_total'] = await getTotal(token)   
    for(var a=0; a < data_products.length;a++){
        data_variants = await getVariantInfo(data_products[a].sku,admin_token); //variants of a product     
        var item = {};
        item['cart_id'] = await data_products[0].quote_id; 
        item['name'] = await data_products[a].name;
        item['qty'] = await data_products[a].qty;
        item['item_id'] = await data_products[a].item_id;
        item['price'] = await total['grand_total'].items[a].base_row_total;
        item['product_option'] = await data_products[a].product_option;
        item['custom_attributes'] = await JSON.parse(data_variants).custom_attributes;
        item['all_info'] = await data_variants;  
        await result['Product_Info'].push(item);     
    } 
    result['Product_Info'].push(total); 
    return result;
}  

async function getTotal(token){

    let response = await requestify.request(constant.CART_TOTAL,
    {
        method : 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }
    );

    let total = JSON.parse(response.body);
    return total;

}

async function getAdminToken(){
    let token =  await requestify.request(constant.ADMIN_URL,{
        method: 'POST',
        body:{
                
                "username" : constant.ADMIN_USERNAME,
	            "password" : constant.ADMIN_PASSWORD
        
        },
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    });

    return token.body.replace(/"/g,'');

}

async function getVariantInfo(sku,token){
   
    let res = await requestify.request(constant.PRODUCT_INFO+sku,{
         method: 'GET',
         headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
             'content-type' : 'application/json; charset=utf-8'
         }
     });  
     return res.body;
}

exports.removeCartItem = function(req,res){

    if(req.body.Authorization != null){
        
        var token = 'Bearer '+req.body.Authorization;  

        requestify.request(constant.USER_CART + req.params.cartId + '/items/' + req.params.itemId,{
                        method: 'DELETE',
                        headers:{
                            'authorization' : 'Bearer '+ token,
                            'content-type' : 'application/json; charset=utf-8'
                        }
        }).then(function(response) {
            res.send(response.body);                            
        }).catch(function(err){
            res.send(err.body);
        });
    }
    else {
        console.log(constant.GUEST_USER_CART + req.params.cartId + '/items/' + req.params.itemId)
        requestify.request(constant.GUEST_USER_CART + req.params.cartId + '/items/' + req.params.itemId,{
            method: 'DELETE',
            headers:{
                'content-type' : 'application/json; charset=utf-8'
            }
        }).then(function(response) {
            res.send(response.body);                            
        }).catch(function(err){
            res.send(err.body);
        });
    }
        

};


exports.getOrderDetails = function(req,res){
    
                requestify.request(constant.ORDER_DETAILS+req.params.id,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
    
 };   

 exports.getOrderDetailsViaEmail = function(req,res){
    
    requestify.request(constant.ALL_ORDERS+req.body.email,{
        method: 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(function(response) {
                res.send(response.body);                            
            })
    .catch(function(err){
        res.send(err);
    });

};   


exports.addToWishlist = function(req,res){

    if(req.headers.authorization != null){

        var token = 'Bearer '+req.headers.authorization;

        requestify.request(constant.USER_CART,{
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            }
        }).then(function(response) {
                    var id = '';
                    if(response.body.id == null)
                        id = response.body;
                    else
                        id = response.body.id;  

                        requestify.request(constant.ADD_WISHLIST+req.params.product,{
                        method: 'POST',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        }
                    }).then(function(response) {
                                res.send(response.body);                            
                            })
                    .catch(function(err){
                        res.send(err.body);
                    });
                })
        .catch(function(err){
            res.send(err);
        });
    }

    else
        res.send({error : "requires user token"});

 };

exports.getWishlist = function(req,res){

   

        requestify.request(constant.USER_CART,{
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            }
        }).then(function(response) {
                    var id = '';
                    if(response.body.id == null)
                        id = response.body;
                    else
                        id = response.body.id;  

                        requestify.request(constant.GET_WISHLIST,{
                        method: 'GET',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        }
                    }).then(function(response) {
                                res.send(response.body);                            
                            })
                    .catch(function(err){
                        res.send(err.body);
                    });
                })
        .catch(function(err){
            res.send(err);
        });


 };

exports.updateWishlist = function(req,res){



        requestify.request(constant.USER_CART,{
            method: 'POST',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            }
        }).then(function(response) {
                    var id = '';
                    if(response.body.id == null)
                        id = response.body;
                    else
                        id = response.body.id;  

                        requestify.request(constant.UPDATE_WISHLIST+req.params.id,{
                        method: 'DELETE',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        }
                    }).then(function(response) {
                                res.send(response.body);                            
                            })
                    .catch(function(err){
                        res.send(err.body);
                    });
                })
        .catch(function(err){
            res.send(err);
        });
};  

exports.subscribeNews = function(req,res){

        // requestify.request(constant.NEWSLETTER_SUBSCRIPTION,{
        // method: 'PUT',
        // headers:{
        //     'authorization' : 'Bearer ' + constant.ACCESS_TOKEN,
        //     'content-type' : 'application/json; charset=utf-8'
        // },
        // body : {
            
        //     "customer": 
        //     {
        //       "email" : req.body.email,
        //       "firstname": req.body.firstname,
        //       "lastname": req.body.lastname,
        //       "store_id": 1,
        //       "website_id": 1,
        //       "extension_attributes": {
        //         "is_subscribed": true
        //       }

        //     }
        //   }
        // }).then(function(response) {
        //             Newsletter.findOne({email:req.body.email},function(err,newsletter){
                        
        //                 if(!newsletter){
        //                     newsletter = new Newsletter();
        //                     newsletter.email = req.body.email;
        //                     newsletter.status = true;
        //                     newsletter.save();
        //                 }

        //                 else{
        //                     newsletter.status = true;
        //                     newsletter.save();
        //                 }

        //             });

        //             res.send(response.body);   

        //         })
        // .catch(function(err){
        //     res.send(err.body);
        // });


        /* sending subscribers email address to mongodb  */
        console.log(req.body)
        let newsletter = new Newsletter({
            email: req.body.email
        })
        console.log(newsletter)
        newsletter.save().then(response => {
            console.log("\x1b[36m", "The response is...")
            console.log(response);
            let coupon = promotionCtrl.addCouponToCart();
            res.send({"email": response, coupon});
        }).catch(err => {
            res.send(err)
        });

 };  

exports.updateCartItem = async function(req,res){

    if(req.body.Authorization != null){

       var token = 'Bearer ' + req.body.Authorization;
       
       requestify.request(constant.USER_CART + req.params.cartId + '/items/' + req.params.itemId , {
                        method: 'PUT',
                        headers:{
                            'authorization' : token,
                            'content-type' : 'application/json; charset=utf-8'
                        },
                        body : {
                            "cartItem": {
                              "qty" : req.body.qty,
                              "item_id" : req.params.itemId,
                              "quote_id" : req.params.cartId
                          }
                        }
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            console.log(err);
            res.send(err);
        }); 
    }
    else{
        requestify.request(constant.GUEST_USER_CART + req.params.cartId + '/items/' + req.params.itemId , {
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8'
            },
            body : {
                "cartItem": {
                  "qty" : req.body.qty,
                  "itemId" : req.params.itemId,
                  "quoteId" : req.params.cartId
              }
            }
            }).then(response => {
                res.send(response.body);
            }).catch(err => {
                console.log(err);
                res.send(err);
            });
    }
 };

exports.unsubscribeNews = function(req,res){

    if(req.headers.authorization != null){

        var token = 'Bearer '+req.headers.authorization;
        requestify.request(constant.NEWSLETTER_SUBSCRIPTION,{
        method: 'PUT',
        headers:{
            'authorization' : token,
            'content-type' : 'application/json; charset=utf-8'
        },
        body : {
            
            "customer": 
            {
              "email" : req.body.email,
              "firstname": req.body.firstname,
              "lastname": req.body.lastname,
              "store_id": 1,
              "website_id": 1,
              "extension_attributes": {
                "is_subscribed": false
              }

            }
          }
        }).then(function(response) {
                    Newsletter.findOne({email:req.body.email},function(err,newsletter){
                                
                        if(!newsletter){
                            newsletter.email = req.body.email;
                            newsletter.status = false;
                            newsletter.save();
                        }

                        else{

                            newsletter.status = false;
                            newsletter.save();
                        }

                    });
                    res.send(response.body);                            
                })
        .catch(function(err){
            res.send(err.body);
        });
    }

    else
        res.send({error : "requires user token"});

 };

exports.getSubscriptionStatus = function(req,res){
    Newsletter.findOne({email:req.body.email},function(err,newsletter){
        
        if(err)
            res.send({message : 'email not found!'});
        
        else {
            res.send({message : newsletter.status});   
        }});
} 




async function countries(country){
    try{
    var countries_ = await JSON.parse(constant.COUNTRIES);  
    for(var a=0; a<countries_.length; a++){
            if(await countries_[a].name.toLowerCase() == country.toLowerCase()){
                return await countries_[a].alpha2Code;
            }
      }
    }
    catch(err){console.log(err);}

}

async function setAddresses(req,street1,street2){
    try{    
    var rough = await {
                
        "addressInformation": {

        "shipping_address": {
             "region": await req.body.shipping_address.city,
             "region_id": 0,
             "region_code": "0",
             "country_id": ""+await countries(await req.body.shipping_address.country),
             "street": await street1,
            "postcode":await req.body.shipping_address.postcode,
            "city":await req.body.shipping_address.city,
            "firstname": await req.body.shipping_address.firstname,
            "lastname":await req.body.shipping_address.lastname,
            "telephone": await req.body.shipping_address.telephone,
            "email" : await req.body.shipping_address.email
        },

        "billing_address": {
          "region":await req.body.billing_address.city,
          "region_id": 0,
          "region_code": "a",
          "country_id": ""+await countries(req.body.billing_address.country),
          "street": await street2,
          "postcode":await req.body.billing_address.postcode,
          "city": await req.body.billing_address.city,
          "firstname":await req.body.billing_address.firstname,
          "lastname":await req.body.billing_address.lastname,
          "telephone":await req.body.billing_address.telephone,
          "email" : await req.body.billing_address.email
        },
        "shipping_carrier_code": "flatrate",
        "shipping_method_code": "flatrate"
        }

      };   
     return await rough; 
    }catch(err){console.log(err);}
}

exports.shippingMethod = async function(req,res){
    if(req.headers.authorization != null){        
        var token = 'Bearer '+req.headers.authorization;
        try{ 
            var street1 = {};
            var street2 = {};
            street1['street'] = [];
            street2['street'] = [];
            await street1['street'].push(req.body.shipping_address.street);
            await street2['street'].push(await req.body.billing_address.street); 
            let result = await setAddresses(req,street1['street'],street2['street']);
            try{
            await requestify.request(constant.SHIPPING_METHOD,{
                method: 'POST',
                headers:{
                    'authorization' : token,
                    'content-type' : 'application/json; charset=utf-8'
                },
                body : await result
            });
            await res.json({message : "Shipping method saved"});
            }catch(err){console.log(err);}    
        } catch(err){res.send(err.body);}           
    }
    else
        res.send({error : "requires user token"});
}

exports.paymentMethod = async function(req,res){
    if(req.headers.authorization != null){        
        var token = 'Bearer '+req.headers.authorization;
        try{ 
            var street = {};
            street['street'] = [];            
            await street['street'].push(req.body.billing_address.street);
            let result = await setPayment(req,street['street']);
            try{
            let response  = await requestify.request(constant.PAYMENT_METHOD,{
                method: 'POST',
                headers:{
                    'authorization' : token,
                    'content-type' : 'application/json; charset=utf-8'
                },
                body : await result
            });
            await res.json({order_id : JSON.parse(response.body)});
            }catch(err){console.log(err);}    
        } catch(err){res.send(err.body);}           
    }
    else
        res.send({error : "requires user token"});
}

async function setPayment(req,street){
    try{    
    var rough = await {

        "paymentMethod": {
            "method": "razorpay"
        },
                        
        "billing_address": {
          "region":await req.body.billing_address.city,
          "region_id": 0,
          "region_code": "a",
          "country_id": ""+await countries(req.body.billing_address.country),
          "street": await street,
          "postcode":await req.body.billing_address.postcode,
          "city": await req.body.billing_address.city,
          "firstname":await req.body.billing_address.firstname,
          "lastname":await req.body.billing_address.lastname,
          "telephone":await req.body.billing_address.telephone,
          "email" : await req.body.billing_address.email
        }
      };   
     return await rough; 
    }catch(err){console.log(err);}
}

// get user payment information
exports.getUserPaymentInformation = (req, res) => {
    requestify.request(constant.GET_USER_CART_PAYMENT_OPTIONS,{
        method: 'GET',
        headers:{
            'content-type' : 'application/json; charset=utf-8',
            'Authorization': 'Bearer ' + req.body.Authorization
        }
    }).then(response => {
        res.send(response.body);
    }).catch(err => {
        console.log(err);
        res.send(err);
    });
}


// guest cart actions

exports.getPaymentInformation = (req, res) => {
    requestify.request(constant.GUEST_USER_CART + req.params.id + constant.PAYMENT_INFO,{
        method: 'GET',
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(response => {
        res.send(response.body);
    }).catch(err => {
        console.log(err);
        res.send(err);
    });
}


exports.cartBillingAddress = (req, res) => {
    let data = setAddressData(req)
    console.log(data)
    requestify.request(constant.GUEST_USER_CART + req.params.cartId + constant.BILLING_ADDRESS, {
        method: 'POST',
        headers: {
            'content-type': 'application/json; charset=utf-8'
        },
        body: data
    }).then(response => {
        res.send(response.body)
    }).catch(err => {
        console.log(err);
        res.send(err);
    })
}


var setAddressData = (req) => {
    let data ={
            "address": {
              "region": req.body.region? req.body.region: "",
              "regionId": 0,
              "regionCode": "string",
              "countryId": "IN",
              "street": req.body.street.split(','),
              "telephone": req.body.phone,
              "postcode": req.body.pincode,
              "city": req.body.city,
              "firstname": req.body.fname,
              "lastname": req.body.lname,
              "email": req.body.email,            
            }
    }
    return data
}

exports.cartShippingAddress = (req, res) => {
    let data = setShippingData(req)
    console.log(data)
    requestify.request(constant.GUEST_USER_CART + req.params.cartId + constant.SHIPPING_INFO, {
        method: 'POST',
        headers: {
            'content-type': 'application/json; charset=utf-8'
        },
        body: data
    }).then(response => {
        res.send(response.body)
    }).catch(err => {
        console.log(err);
        res.send(err);
    })
}

var setShippingData = (req) => {
    let data = { 
        "addressInformation": {
            "shippingAddress": {
                "region":  req.body.region? req.body.region: "",
                "regionId": 0,
                "regionCode": "string",
                "countryId": "IN",
                "street": req.body.street.split(','),
                "telephone": req.body.phone,
                "postcode": req.body.pincode,
                "city": req.body.city,
                "firstname": req.body.fname,
                "lastname": req.body.lname,
                "email": req.body.email,
                "sameAsBilling": 0,
         },
             "shippingMethodCode": "flatrate",
             "shippingCarrierCode": "flatrate"
           }
          }
      return data;    
}

