var request = require('request');
var requestify = require('requestify'); 
const constant = require('./../config');
var Newsletter = require('../models/Newsletter');

exports.addAccount = function(req,res){    
    // let token = req.get('accessToken').replace(/"/g,'');
    let token = config.ACCESS_TOKEN;
                requestify.request(constant.ADD_ACCOUNT,{
                    method: 'POST',
                    headers:{
                        'authorization' : 'Bearer '+token,
                        'content-type' : 'application/json; charset=utf-8'
                    },
                    body : {
                        'customer': {
                            "email" : req.body.email,
                            "firstname" : req.body.firstname,
                            "lastname" : req.body.lastname,
                            "storeId" : 1,
                            "websiteId" : 1
                        },                        
                        "password" : req.body.password
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
          
  };


exports.getAccount = function(req,res){    
    if(req.body.authorization != null){
        // var token = 
        requestify.request(constant.GET_ACCOUNT,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer ' + req.body.authorization,
                'content-type' : 'application/json; charset=utf-8'
            }
        }).then(function(response) {
                    // res.headers('Access-Control-Allow-Headers', 'authorization')
                    res.send(response.body);                            
                })
        .catch(function(err){
            res.send(err);
        });
    }
    else{
        res.json({message : 'User token missing!'});
    }
};

exports.validateAccount = function(req,res){    
    let token = constant.ACCESS_TOKEN;
    // console.log('jdasnkjdansjdniasjndiajsndijasnidnasijn');
    
    requestify.request(constant.AUTHENTICATE_USER,{
        method: 'POST',
        body:{                
                "username" : req.body.email,
	            "password" : req.body.password        
        },
        headers:{
            'authorization' : 'Bearer '+token,
            'content-type' : 'application/json; charset=utf-8'        }
    }).then(function(response) {
                if(response.code == 401)
                    res.json({success : "false"});
                else
                    res.json({success : response.body.replace(/"/g,'')});    
            })
    .catch(function(err){
        if(err.code == 401)
        res.json({success : "false"});
        console.log(err);
    });
  
};

exports.updatePassword = async function(req,res){
    if(req.body.Authorization != null){
        var token = 'Bearer '+req.body.Authorization;
        await res.send(await resetPassword(token,req.body.currentPassword,req.body.newPassword));
    }

    else
      res.send({error : "requires user token"});

  };

async function resetPassword(token,current,new_pass){
    try{        
        let response = await requestify.request(constant.UPDATE_PASSWORD,{
                        method: 'PUT',
                        headers:{
                            'content-type' : 'application/json; charset=utf-8',
                            'authorization' : token
                        },
                        body : {
                            "currentPassword": current,
                            "newPassword": new_pass
                        }
                    });
        return await JSON.parse(response.body);

     }  
     catch(err){return err.body}

}

exports.updateAccount = async function(req,res){
    
    try{
        if(req.body.Authorization != null){
         
         var token = 'Bearer '+req.body.Authorization;
         var result = {};
         result['Password_Change_Status'] = [];
         result['Account_Info'] = [];
         if(req.body.change_password)
          await result['Password_Change_Status'].push(await resetPassword(token,req.body.current,req.body.new_pass));
         else
          await result['Password_Change_Status'].push('false');

        try{

        let response = await requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            },
            body : {                
                "customer" : {
                    "email": req.body.email,
                    "firstname": req.body.firstname,
                    "lastname": req.body.lastname,
                    "storeId": 1, "websiteId": 1
               }
            }
        });
        await result['Account_Info'].push(JSON.parse(response.body));
        await res.send(result);
    }

     catch(err){res.send(err.body);}
    }
    else
        res.json({message : 'User token missing!'});
    }
    catch(err){return err.body;}
}
exports.setAddressAsDefault = (req, res) => {
    if(req.body.Authorization != null){         
        var token = 'Bearer '+req.body.Authorization;     
            let address_response = requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization' : token
            }
        }).then(details => {
            let addresses = JSON.parse(details.body).addresses;
            let newAddresses = [], defaultAddress, addressToBeSetAsDefault;
            
            for (let i = 0; i < addresses.length; i++) {
                // console.log(addresses[i].id === parseInt(req.params.id));
                // console.log(addresses[i]);
                
                if (addresses[i].id === parseInt(req.params.id)) {
                    // console.log("lkdsamkldasmasdlkmadslkm");
                    addressToBeSetAsDefault = addresses[i];
                    addressToBeSetAsDefault.default_billing = true;
                    addressToBeSetAsDefault.default_shipping = true;
                    newAddresses.push(addressToBeSetAsDefault);
                } else if (addresses[i].default_billing) {
                    // console.log("kjdasndkjasfkldasnkj");
                    defaultAddress = addresses[i];
                    delete defaultAddress.default_shipping;
                    delete defaultAddress.default_billing;
                    newAddresses.push(defaultAddress);
                } else {
                    newAddresses.push(addresses[i]);
                }
            }
            console.log({...JSON.parse(details.body), addresses: newAddresses});
            
            requestify.request(constant.UPDATE_ACCOUNT,{
                method: 'PUT',
                headers:{
                    'content-type' : 'application/json; charset=utf-8',
                    'Authorization' : token
                },
                body: {customer: {...JSON.parse(details.body), addresses: newAddresses}}
            }).then(responseAfterUpdating => {
                res.json(JSON.parse(responseAfterUpdating.body));
            })
            


            // res.send();    
        });
    }
    else
        res.json({message : 'User token missing!'});
}

exports.getAddresses = async function(req,res){    
   
    if(req.body.Authorization != null){         
    var token = 'Bearer '+req.body.Authorization;     
    try{
        let address_response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses',{
        method: 'GET',
        headers:{
            'content-type' : 'application/json; charset=utf-8',
            'Authorization' : token
        }
    });  
    await res.send(address_response.body);    
    }
     catch(err){res.send(err.body);}
    }
    else
        res.json({message : 'User token missing!'});
}



exports.accountDashboard = async function(req,res){

    if(req.headers.authorization != null){         
        var token = 'Bearer '+req.headers.authorization;
        var result = {};
        result['subcription_status'] = [];
        result['info'] = [];
        try{
            let response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses,email,firstname,lastname',{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'Authorization' : token
            }
        });  
        let email = await JSON.parse(response.body).email;
        await Newsletter.findOne({email : email},function(err,newsletter){
            if(!err)
                result['subcription_status'].push(newsletter.status);
            else
                result['subcription_status'].push('not found');
        });
        await result['info'].push(await JSON.parse(response.body)); 
        await res.send(await result);    
        }
         catch(err){res.send(err.body);}
        }
        else
            res.json({message : 'User token missing!'});

}

exports.deleteSelectedAddress = async (req, res) => {
    if (req.body.Authorization) {
        let userData = await requestify.request(constant.GET_ACCOUNT,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer ' + req.body.Authorization,
                'content-type' : 'application/json; charset=utf-8'
            }
        })
    
        let addressArr = [];
        console.log(userData);
        
        let idToBeDeleted = parseInt(req.body.addressId);
        for (let i = 0; i < JSON.parse(userData.body).addresses.length; i++) {
            if (JSON.parse(userData.body).addresses[i].id !== idToBeDeleted) {
                addressArr.push(JSON.parse(userData.body).addresses[i]);
            }
        }

        if (addressArr.length === JSON.parse(userData.body).addresses.length) {
            res.send({message: "invalid address id"})
        }
    
        requestify.request(constant.GET_ACCOUNT,{
            method: 'PUT',
            headers:{
                'authorization' : 'Bearer ' + req.body.Authorization,
                'content-type' : 'application/json; charset=utf-8'
            },
            body : {                
                "customer" : {
                    "email": JSON.parse(userData.body).email,
                    "firstname": JSON.parse(userData.body).firstname,
                    "lastname": JSON.parse(userData.body).lastname,
                    "storeId": 1, "websiteId": 1,
                    "addresses": addressArr
               }
            }
        }).then(resAfterDeletion => {
            res.send(resAfterDeletion.body);
        }).catch(err => res.send(err))
    } else {
        res.send("Correct Authorization token is required")
    }
}

exports.editSelectedAddress = async (req, res) => {
    if (req.body.Authorization) {
        let userData = await requestify.request(constant.GET_ACCOUNT,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer ' + req.body.Authorization,
                'content-type' : 'application/json; charset=utf-8'
            }
        })
    
        let addressArr = [];
        let addressToBeEditted = null;
        console.log(userData);
        
        let idToBeDeleted = parseInt(req.body.addressId);
        for (let i = 0; i < JSON.parse(userData.body).addresses.length; i++) {
            console.log(JSON.parse(userData.body).addresses[i].id === idToBeDeleted, 'address ' + i + 1);
            
            if (JSON.parse(userData.body).addresses[i].id === idToBeDeleted) {
                addressToBeEditted = JSON.parse(userData.body).addresses[i]
            } else {
                addressArr.push(JSON.parse(userData.body).addresses[i]);
            }
        }

        console.log(addressToBeEditted, "jaksdnkjasnkjdsan")

        // country_id
        // street
        // telephone
        // postcode
        // city
        // firstname
        // lastname

        if (!addressToBeEditted) {
            res.send({message: "invalid address id"})
            return;
        }
        addressToBeEditted.country_id = req.body.country ? req.body.country : addressToBeEditted.country_id;
        addressToBeEditted.street = req.body.street ? req.body.street : addressToBeEditted.street;
        addressToBeEditted.telephone = req.body.telephone ? req.body.telephone : addressToBeEditted.telephone;
        addressToBeEditted.postcode = req.body.postcode ? req.body.postcode : addressToBeEditted.postcode;
        addressToBeEditted.city = req.body.city ? req.body.city : addressToBeEditted.city;
        addressToBeEditted.firstname = req.body.firstname ? req.body.firstname : addressToBeEditted.firstname;
        addressToBeEditted.lastname = req.body.lastname ? req.body.lastname : addressToBeEditted.lastname;

        addressArr.push(addressToBeEditted);

        requestify.request(constant.GET_ACCOUNT,{
            method: 'PUT',
            headers:{
                'authorization' : 'Bearer ' + req.body.Authorization,
                'content-type' : 'application/json; charset=utf-8'
            },
            body : {                
                "customer" : {
                    "email": JSON.parse(userData.body).email,
                    "firstname": JSON.parse(userData.body).firstname,
                    "lastname": JSON.parse(userData.body).lastname,
                    "storeId": 1, "websiteId": 1,
                    "addresses": addressArr
               }
            }
        }).then(resAfterDeletion => {
            res.send(resAfterDeletion.body);
        }).catch(err => res.send(err))
    } else {
        res.send("Correct Authorization token is required")
    }
}


exports.deleteAddress = async function(req,res){
    requestify.request(constant.ADMIN_URL,{
        method: 'POST',
        body:{                
            "username" : constant.ADMIN_USERNAME,
            "password" : constant.ADMIN_PASSWORD        
        },
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(function(response) {
                var token = response.body.replace(/"/g,'');
                requestify.request(constant.DELETE_ADDRESS+req.params.address,{
                    method: 'DELETE',
                    headers:{
                        'authorization' : 'Bearer '+token,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err.body);
                });
            })
    .catch(function(err){
        res.send(err.body);
    });
}

exports.updateAddress = async function(req,res) {    
    
    if(req.headers.authorization != null){         
        var token = 'Bearer '+req.headers.authorization;     
        try{
            let address_response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses,email,firstname,lastname,store_id,website_id',{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            }
        });  

        var address = await JSON.parse(address_response.body);
        for(var a=0; a<address.addresses.length;a++){
               
            if(address.addresses[a].id == req.body.id){
                    
                    address.addresses[a].country_id = req.body.country;
                    address.addresses[a].street = [
                        req.body.street
                    ];
                    address.addresses[a].firstname = req.body.firstname;
                    address.addresses[a].lastname = req.body.lastname;
                    address.addresses[a].telephone = req.body.telephone;
                    address.addresses[a].postcode = req.body.postcode;
                    address.addresses[a].city = req.body.city;
                    if(req.body.default_shipping == true)
                        address.addresses[a].default_shipping = true;
                    if(req.body.default_billing == true)    
                        address.addresses[a].default_billing = true;
                    
            }
        }
        
        var send = {};
        send["customer"] = [];
        send["customer"] = await address;
        let result = await requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            },
            body : await send
        });          
            await res.send(await JSON.parse(result.body));        
        }
         catch(err){res.send(err.body);}
        }
        else
            res.json({message : 'User token missing!'});

}

exports.resetPasswordEmail = function(req,res){
    requestify.request(constant.ADMIN_URL,{
        method: 'POST',
        body:{                
                "username" : constant.ADMIN_USERNAME,
	            "password" : constant.ADMIN_PASSWORD
            },
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(function(response) {
                var token = response.body.replace(/"/g,'');
                requestify.request(constant.RESET_PASSWORD,{
                    method: 'PUT',
                    headers:{
                        'authorization' : 'Bearer '+token,
                        'content-type' : 'application/json; charset=utf-8'
                    },
                    body : {
                        "email" : req.body.email,
                        "template" : "email_reset"
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err.body);
                });
            })
    .catch(function(err){
        res.send(err.body);
    });
};

exports.getCountries = function(req,res){
  res.send(JSON.parse(constant.COUNTRIES));
}

exports.addNewAddress = async function(req,res) {    
    
    if(req.body.Authorization != null){         
        var token = 'Bearer '+req.body.Authorization;     
        try{
            let address_response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses,email,firstname,lastname,store_id,website_id',{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            }
        });  

        var address = await JSON.parse(address_response.body);

        var new_address = await {
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": req.body.country,
            "street": [
                req.body.street
            ],
            "telephone": req.body.telephone,
            "postcode": req.body.postcode,
            "city": req.body.city,
            "firstname": req.body.firstname,
            "lastname": req.body.lastname
        };    

        await address.addresses.push(await new_address);
        console.log(await address);
        var send = {};
        send["customer"] = [];
        send["customer"] = await address;

        let result = await requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            },
            body : await send
        });          
            await res.send(await JSON.parse(result.body));        
        }
         catch(err){res.send(err.body);}
        }
        else{

            res.send("")

        }
            

}

exports.addNewDefaultAddress =  async function(req,res) {    
    
    if(req.body.Authorization != null){         
        var token = 'Bearer '+req.body.Authorization;     
        try{
            let address_response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses,email,firstname,lastname,store_id,website_id',{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            }
        });  

        var address = await JSON.parse(address_response.body);

        var new_address = {
            "region": {
                "region_code": "string",
                "region": "string",
                "region_id": 0
            },
            "region_id": 0,
            "country_id": req.body.country,
            "street": [
                req.body.street
            ],
            "telephone": req.body.telephone,
            "postcode": req.body.postcode,
            "city": req.body.city,
            "firstname": req.body.firstname,
            "lastname": req.body.lastname,
            "default_shipping": true,
            "default_billing": true
        };    

        await address.addresses.push(await new_address);
        console.log(await address);
        var send = {};
        send["customer"] = [];
        send["customer"] = await address;

        let result = await requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            },
            body : await send
        });          
            await res.send(await JSON.parse(result.body));        
        }
         catch(err){res.send(err.body);}
        }
        else
            res.json({message : 'User token missing!'});

}

exports.changeDefaultAddress = async function(req,res) {    
    
    if(req.body.Authorization != null){         
        var token = 'Bearer '+req.body.Authorization;     
        try{
            let address_response = await requestify.request(constant.UPDATE_ACCOUNT+'?fields=addresses,email,firstname,lastname,store_id,website_id',{
            method: 'GET',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            }
        });  

        var address = await JSON.parse(address_response.body);
        for(var a=0; a<address.addresses.length;a++){
               
            if(address.addresses[a].id == req.params.id){
                    address.addresses[a].default_shipping = true;
                    address.addresses[a].default_billing = true;                    
            }
        }
        
        var send = {};
        send["customer"] = [];
        send["customer"] = await address;
        let result = await requestify.request(constant.UPDATE_ACCOUNT,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8',
                'authorization' : token
            },
            body : await send
        });          
            await res.send(await JSON.parse(result.body));        
        }
         catch(err){res.send(err.body);}
        }
        else
            res.json({message : 'User token missing!'});

}



