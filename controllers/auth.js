var requestify = require('requestify'); 


exports.getAccessToken = (config, constant) => {
    console.log("inside access token route")
    return new Promise((resolve, reject) => {
        requestify.request(constant.ADMIN_URL, {
            method: 'POST',
            body:{
                
                "username" : config.ADMIN_USERNAME,
	            "password" : config.ADMIN_PASSWORD
        
            },
            headers:{
                'content-type' : 'application/json; charset=utf-8'
            }
        })
        .then((response) => {
            resolve(response.body.replace(/"/g,''))
        })
        .catch((err) => {
            reject(err)
        })

})
}

// exports.getAccessToken = (req, res) => {
//     console.log("inside access token route")
    
//     requestify.request(constant.ADMIN_URL, {
//         method: 'POST',
//         body:{
                
//                 "username" : constant.ADMIN_USERNAME,
// 	            "password" : constant.ADMIN_PASSWORD
        
//         },
//         headers:{
//             'content-type' : 'application/json; charset=utf-8'
//         }
//     }).then((response) => {
//         res.send({
//             "access_token": response.body.replace(/"/g,'')
//         })
//     }).catch(err => {
//         console.log(err);
//         res.send(err);
//     })
// }