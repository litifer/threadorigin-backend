var request = require('request');
var requestify = require('requestify'); 
var constant = require('../config');

exports.getCategories = function(req,res){
                requestify.request(constant.CATEGORIES_URL,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
       
  };

exports.getCategoryCamp = function(req,res){
                requestify.request(constant.CATEGORY_CAMP_1,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            var result = {};
                            result["items"] = [];
                            result["items"].push(JSON.parse(response.body));
                            res.send(result);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
           
  };  

exports.sortCategoryColor = function(req,res){
                requestify.request(constant.PRODUCT_SORT_COLOR+req.params.value,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err.body);
                });
            
};  

exports.sortCategorySize = function(req,res){
                requestify.request(constant.PRODUCT_SORT_COLOR+req.params.value,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
    
};    


// exports.getCampaignLooks = (req,res) => {
//     requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS,{
//         method: 'GET',
//         headers:{
//             'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//             'content-type' : 'application/json; charset=utf-8'
//         }
//     }).then(async resCategoryIds => {
//         // res.json(JSON.parse(resCategoryIds.body));
//         // console.log(JSON.parse(resCategoryIds.body))
//         let listLookbookIds = [];
//         for (let i = 0; i < JSON.parse(resCategoryIds.body).children_data.length; i++) {
//             let catObj = JSON.parse(resCategoryIds.body).children_data[i];
//             // console.log(catObj, "catObj");
            
//             if (catObj.name === 'CampaignLooks') {
                
//                 // console.log(catObj.children_data[i], "catObj");
//                 for (let j = 0; j < parseInt(catObj.children_data.length); j++) {
//                     let campLook = catObj.children_data[j];
//                     listLookbookIds.push(parseInt(campLook.id));
                
//                 }
//                 break; 
//             }
//         }
//         console.log(listLookbookIds);
        

//         let data = [];

//         for (let i = 0; i < listLookbookIds.length; i++) {
//             let campaignLook = await requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${listLookbookIds[i]}/products`,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })

//             console.log(constant.GET_IMAGES);
            
//             let prod1 = await requestify.request(constant.GET_IMAGES + '/' + JSON.parse(campaignLook.body)[0].sku,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })
//             let prod2 = await requestify.request(constant.GET_IMAGES + '/' + JSON.parse(campaignLook.body)[1].sku,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })
//             console.log(prod1.body, "prod1");
//             let finalProd1 = {
//                 sku: JSON.parse(campaignLook.body)[0].sku,
//                 imgBig: JSON.parse(prod1.body)[0].file,
//                 imgSmall: JSON.parse(prod1.body)[1].file
//             }
//             let finalProd2 = {
//                 sku: JSON.parse(campaignLook.body)[1].sku,
//                 imgBig: JSON.parse(prod2.body)[0].file,
//                 imgSmall: JSON.parse(prod2.body)[1].file
//             }
//                 data.push({product1: finalProd1, product2: finalProd2});
//         }
//         res.json(data);
        

//     })
//     .catch(function(err){
//         res.send(err.body);
//     });

// };  
exports.getCampaignLooks = (req,res) => {
    requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS,{
        method: 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(async resCategoryIds => {
        // res.json(JSON.parse(resCategoryIds.body));
        // console.log(JSON.parse(resCategoryIds.body))
        let listLookbookIds = [];
        for (let i = 0; i < JSON.parse(resCategoryIds.body).children_data.length; i++) {
            let catObj = JSON.parse(resCategoryIds.body).children_data[i];
            // console.log(catObj, "catObj");
            
            if (catObj.name === 'CampaignLooks') {
                
                // console.log(catObj.children_data[i], "catObj");
                for (let j = 0; j < parseInt(catObj.children_data.length); j++) {
                    let campLook = catObj.children_data[j];
                    listLookbookIds.push(parseInt(campLook.id));
                
                }
                break; 
            }
        }
        console.log(listLookbookIds);
        

        let data = [];
        let campaignLookPromises = [];
        for (let i = 0; i < listLookbookIds.length; i++) {
            let campaignLookPromise = requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${listLookbookIds[i]}/products`,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })


            campaignLookPromises.push(campaignLookPromise);
            
        }

        Promise.all(campaignLookPromises).then(values => {
            // console.log(values, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
            for (let i = 0; i < values.length; i++) {
                let campaignLook = values[i].body;
                console.log(campaignLook, "============================================");
                
                let prod1 = requestify.request(constant.GET_IMAGES + '/' + JSON.parse(campaignLook)[0].sku,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                })
                let prod2 = requestify.request(constant.GET_IMAGES + '/' + JSON.parse(campaignLook)[1].sku,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                })
                Promise.all([prod1, prod2]).then(prodValues => {

                    let jsonProd1 = JSON.parse(prodValues[0].body);
                    let jsonProd2 = JSON.parse(prodValues[1].body);

                    console.log(jsonProd1, "prod1");
                    let finalProd1 = {
                        sku: JSON.parse(campaignLook)[0].sku,
                        imgBig: jsonProd1[0].file,
                        imgSmall: jsonProd1[1].file
                    }
                    let finalProd2 = {
                        sku: JSON.parse(campaignLook)[1].sku,
                        imgBig: jsonProd2[0].file,
                        imgSmall: jsonProd2[1].file
                    }
                    data.push({product1: finalProd1, product2: finalProd2});
                    if (data.length === values.length) {
                        res.json(data)
                    }
                })
            }

        })
        

    })
    .catch(function(err){
        res.send(err.body);
    });

};    
const getDesignerProducts = async (cat) => {

    let data = [];
    
        let campaignLook = await requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${cat}/products`,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                'content-type' : 'application/json; charset=utf-8'
            }
        })
        let finalProduct = {};
        let prodInd = 1;
        let productsOfThisObj = [], designerOfThisLookbook = null, detailsOfThisLookbook = null;
        for (let j = 0; j < JSON.parse(campaignLook.body).length; j++) {
            let prodNow = JSON.parse(campaignLook.body)[j];
            if (prodNow.sku.substring(0, 2).toLowerCase() !== 'lb' && prodNow.sku.substring(0, 8).toLowerCase() !== 'designer') {
                productsOfThisObj.push(prodNow.sku);
            } else {
                if (prodNow.sku.substring(0, 2).toLowerCase() === 'lb') {
                    detailsOfThisLookbook = prodNow.sku;
                } else {
                    designerOfThisLookbook = prodNow.sku;
                }
            }
        }

        // console.log(productsOfThisObj, "products" + i);
        // console.log(designerOfThisLookbook, "des" + i);
        // console.log(detailsOfThisLookbook, "det" + i);

        let finalProductArr = [];

        for (let j = 0; j < productsOfThisObj.length; j++) {
            let productImages = await requestify.request(constant.GET_IMAGES + '/' + productsOfThisObj[j],{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })
            let finalProduct = {
                sku: productsOfThisObj[j],
                imgBig: JSON.parse(productImages.body)[0].file,
                imgSmall: JSON.parse(productImages.body)[1].file
            }
            finalProductArr.push(finalProduct);    
        }

    return finalProductArr;
}

const getDesignerLookbooks = async (listLookbookIds) => {

    let data = [];

    for (let i = 0; i < listLookbookIds.length; i++) {
        let campaignLook = await requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${listLookbookIds[i]}/products`,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                'content-type' : 'application/json; charset=utf-8'
            }
        })
        let finalProduct = {};
        let prodInd = 1;
        let productsOfThisObj = [], designerOfThisLookbook = null, detailsOfThisLookbook = null;
        for (let j = 0; j < JSON.parse(campaignLook.body).length; j++) {
            let prodNow = JSON.parse(campaignLook.body)[j];
            if (prodNow.sku.substring(0, 2).toLowerCase() !== 'lb' && prodNow.sku.substring(0, 8).toLowerCase() !== 'designer') {
                productsOfThisObj.push(prodNow.sku);
            } else {
                if (prodNow.sku.substring(0, 2).toLowerCase() === 'lb') {
                    detailsOfThisLookbook = prodNow.sku;
                } else {
                    designerOfThisLookbook = prodNow.sku;
                }
            }
        }

        // console.log(productsOfThisObj, "products" + i);
        // console.log(designerOfThisLookbook, "des" + i);
        // console.log(detailsOfThisLookbook, "det" + i);

        let finalProductArr = [];

        for (let j = 0; j < productsOfThisObj.length; j++) {
            let productImages = await requestify.request(constant.GET_IMAGES + '/' + productsOfThisObj[j],{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })
            let finalProduct = {
                sku: productsOfThisObj[j],
                imgBig: JSON.parse(productImages.body)[0].file,
                imgSmall: JSON.parse(productImages.body)[1].file
            }
            finalProductArr.push(finalProduct);    
        }

        let descriptionLookbookObj = await requestify.request(constant.GET_LOOKBOOK_DESC + '/' + detailsOfThisLookbook,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                'content-type' : 'application/json; charset=utf-8'
            }
        })

        descriptionLookbookObj = JSON.parse(descriptionLookbookObj.body);


        let finalLookbookDesc = {
            name: descriptionLookbookObj.name
        }

        for (let j = 0; j < descriptionLookbookObj.custom_attributes.length; j++) {
            if (descriptionLookbookObj.custom_attributes[j].attribute_code === 'description') {
                finalLookbookDesc.mainDesc = descriptionLookbookObj.custom_attributes[j].value;
            } else if (descriptionLookbookObj.custom_attributes[j].attribute_code === 'short_description') {
                finalLookbookDesc.shortDesc = descriptionLookbookObj.custom_attributes[j].value;
            }
        }

        let designerLookbookObj = await requestify.request(constant.GET_LOOKBOOK_DESC + '/' + designerOfThisLookbook,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                'content-type' : 'application/json; charset=utf-8'
            }
        })

        let designerImage = await requestify.request(constant.GET_IMAGES + '/' + designerOfThisLookbook,{
            method: 'GET',
            headers:{
                'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                'content-type' : 'application/json; charset=utf-8'
            }
        })

        designerLookbookObj = JSON.parse(designerLookbookObj.body);
        designerImage = JSON.parse(designerImage.body);
        let finalDesigner = {name: designerLookbookObj.name, image: designerImage[0].file};
        


        console.log({
            products: finalProductArr,
            designer: finalDesigner,
            description: finalLookbookDesc,
        }, "final");
        console.log(finalProductArr, "desc");
        console.log(finalDesigner, "design");
        
        data.push({
            products: finalProductArr,
            designer: finalDesigner,
            description: finalLookbookDesc,
        });
    }
    return data;
}

exports.getProductsAndLookbookData = async (req, res) => {
    let sku = req.params.id;
    // console.log(sku);
    
    requestify.request(constant.PRODUCT_INFO + sku,{
        method: 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(async desginerData1 => {

        
        let desginerData = JSON.parse(desginerData1.body);

        let finalDesginerData = {}
    // name: '',
    //     description: '',
    //     shortDescription: '',
    //     sku: '',
    //     image: '',
    //     categoriesAssociated: []

        

        finalDesginerData.name = desginerData.name;
        finalDesginerData.sku = desginerData.sku;
        // finalDesginerData.categoryId = 
        for (let i = 0; i < desginerData.custom_attributes.length; i++) {
            let customAtt = desginerData.custom_attributes[i];
            // console.log(customAtt);
            if (customAtt.attribute_code === "description") {
                finalDesginerData.description = customAtt.value;
            } else if (customAtt.attribute_code === "short_description") {
                finalDesginerData.shortDescription = customAtt.value;
            } else if (customAtt.attribute_code === "image") {
                finalDesginerData.image = customAtt.value;
            } else if (customAtt.attribute_code === "category_ids") {
                finalDesginerData.categoriesAssociated = customAtt.value;
            }
        }

        finalDesginerData.lookbooks = [];
        finalDesginerData.products = [];

        // console.log(finalDesginerData.categoriesAssociated)

        let lookbookAndProductIds = [...finalDesginerData.categoriesAssociated];

        let products = [], lookbooks = [], currentLookbook;
        for (let i = 0; i < lookbookAndProductIds.length; i++) {
            let anonyData = await requestify.request(constant.CATEGORIES_URL + `/${lookbookAndProductIds[i]}/products`,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })
            console.log();
            let cleanData = JSON.parse(anonyData.body);

            let isLookbook = false;
            for (let j = 0; j < cleanData.length; j++) {
                    if (cleanData[j].sku.substring(0, 2).toLowerCase() === 'lb'){
                    isLookbook = true;
                    break;
                }
            }

            if (isLookbook) {
                lookbooks.push(lookbookAndProductIds[i]); 
            } else {
                products = lookbookAndProductIds[i];
            }

        }

        console.log(lookbooks);

        let lookbooksDatas = await getDesignerLookbooks(lookbooks)
        let productsDatas = await getDesignerProducts(products)
        console.log(productsDatas, "-------------------------------------------------------")

        finalDesginerData.products = productsDatas;
        finalDesginerData.lookbooks = lookbooksDatas;

        res.json(finalDesginerData)

    })
}

exports.getDesignerData = (req, res) => {
    requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS,{
        method: 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(async resCategoryIds => {
        let listDesignerIds = {};
        let finalDesignerDataArr = [];
        for (let i = 0; i < JSON.parse(resCategoryIds.body).children_data.length; i++) {
            let catObj = JSON.parse(resCategoryIds.body).children_data[i];
            // console.log(catObj, "catObj");
            
            if (catObj.name === 'Designers') {
                
                // console.log(catObj.children_data.length, "catObj");
                for (let j = 0; j < parseInt(catObj.children_data.length); j++) {
                    let campLook = catObj.children_data[j];
                    // console.log(campLook, "camplook");
                    listDesignerIds[campLook.name] = (parseInt(campLook.id));
                    
                }
                break; 
            }
        }

        console.log(listDesignerIds);
        

        let designerKeys = Object.keys(listDesignerIds);
        for (let i = 0; i < designerKeys.length; i++) {
            console.log(constant.GET_DESIGNER_DETAILS + `/${encodeURI(designerKeys[i])}`);
            let desginerData = await requestify.request(constant.GET_DESIGNER_DETAILS + `${encodeURI(designerKeys[i])}`,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })
            
            desginerData = JSON.parse(desginerData.body).items[0].sku;

            
            desginerData = await requestify.request(constant.PRODUCT_INFO + desginerData,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })

            desginerData = JSON.parse(desginerData.body);

            let finalDesginerData = {}
            // name: '',
            //     description: '',
            //     shortDescription: '',
            //     sku: '',
            //     image: '',
            //     categoriesAssociated: []

            finalDesginerData.name = desginerData.name;
            finalDesginerData.sku = desginerData.sku;
            finalDesginerData.categoryId = listDesignerIds[designerKeys[i]]
            for (let j = 0; j < desginerData.custom_attributes.length; j++) {
                let customAtt = desginerData.custom_attributes[j];
                if (customAtt.attribute_code === "description") {
                    finalDesginerData.description = customAtt.value;
                } else if (customAtt.attribute_code === "short_description") {
                    finalDesginerData.shortDescription = customAtt.value;
                } else if (customAtt.attribute_code === "image") {
                    finalDesginerData.image = customAtt.value;
                } else if (customAtt.attribute_code === "category_ids") {
                    finalDesginerData.categoriesAssociated = customAtt.value;
                }

            
            }

            finalDesignerDataArr.push(finalDesginerData);
            console.log(finalDesignerDataArr);
        }

        res.json(finalDesignerDataArr)

    })
    .catch(function(err){
        res.send(err);
    });
}

// exports.getLookbooks = (req,res) => {
//     requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS,{
//         method: 'GET',
//         headers:{
//             'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//             'content-type' : 'application/json; charset=utf-8'
//         }
//     }).then(async resCategoryIds => {
//         // res.json(JSON.parse(resCategoryIds.body));
//         let listLookbookIds = [];
//         for (let i = 0; i < JSON.parse(resCategoryIds.body).children_data.length; i++) {
//             let catObj = JSON.parse(resCategoryIds.body).children_data[i];
//             // console.log(catObj, "catObj");
            
//             if (catObj.name === 'Lookbook') {
                
//                 // console.log(catObj.children_data.length, "catObj");
//                 for (let j = 0; j < parseInt(catObj.children_data.length); j++) {
//                     let campLook = catObj.children_data[j];
//                     // console.log(campLook, "camplook");
//                     listLookbookIds.push(parseInt(campLook.id));
                
//                 }
//                 break; 
//             }
//         }

//         let data = [];

//         for (let i = 0; i < listLookbookIds.length; i++) {
//             let campaignLook = await requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${listLookbookIds[i]}/products`,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })
//             let finalProduct = {};
//             let prodInd = 1;
//             let productsOfThisObj = [], designerOfThisLookbook = null, detailsOfThisLookbook = null;
//             for (let j = 0; j < JSON.parse(campaignLook.body).length; j++) {
//                 let prodNow = JSON.parse(campaignLook.body)[j];
//                 if (prodNow.sku.substring(0, 2).toLowerCase() !== 'lb' && prodNow.sku.substring(0, 8).toLowerCase() !== 'designer') {
//                     productsOfThisObj.push(prodNow.sku);
//                 } else {
//                     if (prodNow.sku.substring(0, 2).toLowerCase() === 'lb') {
//                         detailsOfThisLookbook = prodNow.sku;
//                     } else {
//                         designerOfThisLookbook = prodNow.sku;
//                     }
//                 }
//             }

//             // console.log(productsOfThisObj, "products" + i);
//             // console.log(designerOfThisLookbook, "des" + i);
//             // console.log(detailsOfThisLookbook, "det" + i);

//             let finalProductArr = [];

//             for (let j = 0; j < productsOfThisObj.length; j++) {
//                 let productImages = await requestify.request(constant.GET_IMAGES + '/' + productsOfThisObj[j],{
//                     method: 'GET',
//                     headers:{
//                         'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                         'content-type' : 'application/json; charset=utf-8'
//                     }
//                 })
//                 let finalProduct = {
//                     sku: productsOfThisObj[j],
//                     imgBig: JSON.parse(productImages.body)[0].file,
//                     imgSmall: JSON.parse(productImages.body)[1].file
//                 }
//                 finalProductArr.push(finalProduct);    
//             }

//             let descriptionLookbookObj = await requestify.request(constant.GET_LOOKBOOK_DESC + '/' + detailsOfThisLookbook,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })

//             descriptionLookbookObj = JSON.parse(descriptionLookbookObj.body);


//             let finalLookbookDesc = {
//                 name: descriptionLookbookObj.name
//             }

//             for (let j = 0; j < descriptionLookbookObj.custom_attributes.length; j++) {
//                 if (descriptionLookbookObj.custom_attributes[j].attribute_code === 'description') {
//                     finalLookbookDesc.mainDesc = descriptionLookbookObj.custom_attributes[j].value;
//                 } else if (descriptionLookbookObj.custom_attributes[j].attribute_code === 'short_description') {
//                     finalLookbookDesc.shortDesc = descriptionLookbookObj.custom_attributes[j].value;
//                 }
//             }

            // let designerLookbookObj = await requestify.request(constant.GET_LOOKBOOK_DESC + '/' + designerOfThisLookbook,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })

//             let designerImage = await requestify.request(constant.GET_IMAGES + '/' + designerOfThisLookbook,{
//                 method: 'GET',
//                 headers:{
//                     'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//                     'content-type' : 'application/json; charset=utf-8'
//                 }
//             })

//             designerLookbookObj = JSON.parse(designerLookbookObj.body);
//             designerImage = JSON.parse(designerImage.body);
//             let finalDesigner = {name: designerLookbookObj.name, image: designerImage[0].file};
            


//             console.log({
//                 products: finalProductArr,
//                 designer: finalDesigner,
//                 description: finalLookbookDesc,
//             }, "final");
//             console.log(finalProductArr, "desc");
//             console.log(finalDesigner, "design");
            
//             data.push({
//                 products: finalProductArr,
//                 designer: finalDesigner,
//                 description: finalLookbookDesc,
//             });
            
//             // for (let j = 0; j < campaignLook.length; j++) {
//             //     if ()
//             // }
//             // let prod4 = await requestify.request(constant.GET_IMAGES + '/' + JSON.parse(campaignLook.body)[3].sku,{
//             //     method: 'GET',
//             //     headers:{
//             //         'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
//             //         'content-type' : 'application/json; charset=utf-8'
//             //     }
//             // })
//             // console.log(prod1.body, "prod1");
//             // let finalProd4 = {
//             //     sku: JSON.parse(campaignLook.body)[3].sku,
//             //     imgBig: JSON.parse(prod4.body)[0].file,
//             //     imgSmall: JSON.parse(prod4.body)[1].file
//             // }
//             // console.log({product1: finalProd1, 
//             //     product2: finalProd2,
//             //     product3: finalProd3,
//             //     product4: finalProd4,
//             // });
            
//                 // data.push({product1: finalProd1, 
//                 //     product2: finalProd2,
//                 //     product3: finalProd3,
//                 //     product4: finalProd4,
//                 // });
//                 // console.log(data, "array data")
//         }
//         res.json(data);
        

//     })
//     .catch(function(err){
//         res.send(err);
//     });

// };    

exports.getLookbooks = (req,res) => {
    requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS,{
        method: 'GET',
        headers:{
            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(async resCategoryIds => {
        
        let listLookbookIds = [];
        for (let i = 0; i < JSON.parse(resCategoryIds.body).children_data.length; i++) {
            let catObj = JSON.parse(resCategoryIds.body).children_data[i];
            // console.log(catObj, "catObj");
            
            if (catObj.name === 'Lookbook') {
                
                // console.log(catObj.children_data.length, "catObj");
                for (let j = 0; j < parseInt(catObj.children_data.length); j++) {
                    let campLook = catObj.children_data[j];
                    // console.log(campLook, "camplook");
                    listLookbookIds.push(parseInt(campLook.id));
                
                }
                break; 
            }
        }
        let lookbookProductDataPromises = [];
        for (let i = 0; i < listLookbookIds.length; i++) {
            let curId = listLookbookIds[i];
            let campaignLookProducts = requestify.request(constant.GET_LOOKBOOK_CATEGORY_IDS + `/${listLookbookIds[i]}/products`,{
                method: 'GET',
                headers:{
                    'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                    'content-type' : 'application/json; charset=utf-8'
                }
            })
            lookbookProductDataPromises.push(campaignLookProducts);
        }
        Promise.all(lookbookProductDataPromises).then(prodValuesArr => {
            let prodDataOfLookbooks = [];
            for (let j = 0; j < prodValuesArr.length; j++) {
                let filteredProdsOfCurrentLookbook = JSON.parse(prodValuesArr[j].body);
                // console.log(filteredProdsOfCurrentLookbook);
                let productsOfThisObj = [], detailsOfThisLookbook = null, designerOfThisLookbook = null;
                for (let k = 0; k < filteredProdsOfCurrentLookbook.length; k++) {
                    let prodNow = filteredProdsOfCurrentLookbook[k];
                    if (prodNow.sku.substring(0, 2).toLowerCase() !== 'lb' && prodNow.sku.substring(0, 8).toLowerCase() !== 'designer') {
                        productsOfThisObj.push(prodNow.sku);
                    } else {
                        if (prodNow.sku.substring(0, 2).toLowerCase() === 'lb') {
                            detailsOfThisLookbook = prodNow.sku;
                        } else {
                            designerOfThisLookbook = prodNow.sku;
                        }
                    }                     
                }
                prodDataOfLookbooks.push({products: productsOfThisObj, designer: designerOfThisLookbook, description: detailsOfThisLookbook})
            }
            let finalLookbook = [];
            for (let j = 0; j < prodDataOfLookbooks.length; j++) {
                let curLookbook = prodDataOfLookbooks[j];
                let prodOfThisLookbookPromises = [];
                for (let k = 0; k < curLookbook.products.length; k++) {
                    let prodImgs = requestify.request(constant.GET_IMAGES + '/' + curLookbook.products[k],{
                        method: 'GET',
                        headers:{
                            'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                            'content-type' : 'application/json; charset=utf-8'
                        }
                    });
                    prodOfThisLookbookPromises.push(prodImgs);                      
                }
                let designerDataPromise = requestify.request(constant.GET_LOOKBOOK_DESC + '/' + curLookbook.designer,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                });
                let designerImagePromise = requestify.request(constant.GET_IMAGES + '/' + curLookbook.designer,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                });
                let descriptionDataPromise = requestify.request(constant.GET_LOOKBOOK_DESC + '/' + curLookbook.description,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                });

                prodOfThisLookbookPromises.push(designerDataPromise);
                prodOfThisLookbookPromises.push(designerImagePromise);
                prodOfThisLookbookPromises.push(descriptionDataPromise);

                let productsOfThisLookbook = [], designer = null, description = null;
                Promise.all(prodOfThisLookbookPromises).then(allData => {
                    for (let k = 0; k < allData.length - 3; k++) {
                        let prodNow = JSON.parse(allData[k].body);
                        // finalLookbook.push(prodNow);
                        console.log(prodNow, "#####################################################");
                        productsOfThisLookbook.push({sku: curLookbook.products[k], imgSmall: prodNow[0].file, imgBig: prodNow[1].file});
                        // prodDataOfLookbooks.push({});
                    }
                    let receivedDesignerData = JSON.parse(allData[allData.length - 3].body),
                    receivedDesignerImage = JSON.parse(allData[allData.length - 2].body),
                    receivedDescription = JSON.parse(allData[allData.length - 1].body);
                    designer = {name: receivedDesignerData.name, image: receivedDesignerImage[0].file}

                    description = {
                        name: receivedDescription.name
                    }
            
                    for (let j = 0; j < receivedDescription.custom_attributes.length; j++) {
                        if (receivedDescription.custom_attributes[j].attribute_code === 'description') {
                            description.mainDesc = receivedDescription.custom_attributes[j].value;
                        } else if (receivedDescription.custom_attributes[j].attribute_code === 'short_description') {
                            description.shortDesc = receivedDescription.custom_attributes[j].value;
                        }
                    }

                    finalLookbook.push({products: productsOfThisLookbook, designer, description});
                    if (finalLookbook.length === prodValuesArr.length) {
                        res.json(finalLookbook);
                    }
                })
            }
            // res.json(prodDataOfLookbooks);
        })


    })
    .catch(function(err){
        res.send(err);
    });

};    

