var requestify = require('requestify'); 
var constant = require('../config');
var Instagram = require('instagram-node').instagram();



exports.getExclusives = function(req,res){
    
                requestify.request(constant.EXCLUSIVE_URL,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
          
  };


exports.getMediaURL= function(req,res){
    res.send(constant.MEDIA_BASE_URL);
}  



exports.getCamp1 = function(req,res){
                
                requestify.request(constant.HOME_CAMP_1,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    
                    res.send(err);
                });
           
  };


  exports.getCamp2 = function(req,res){
                requestify.request(constant.HOME_CAMP_2,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
  }; 


exports.getCamp3 = function(req,res){
    
                requestify.request(constant.HOME_CAMP_3,{
                    method: 'GET',
                    headers:{
                        'authorization' : 'Bearer '+ constant.ACCESS_TOKEN,
                        'content-type' : 'application/json; charset=utf-8'
                    }
                }).then(function(response) {
                            res.send(response.body);                            
                        })
                .catch(function(err){
                    res.send(err);
                });
            
  }; 


exports.instaFeed = function(req,res){
    //8006095402.8a540c5.df7cf68e50b241c39ea3dd6463452527
    Instagram.use({ access_token: '7800512542.e6721fc.3204dd3f131c4e97b340d4277be495f8' });

    Instagram.user_self_media_recent(function(err, medias, pagination, remaining, limit) {
            if(err){
                res.json({message : 'authentication failed!'});
            }
            else{
                res.send(medias);
            }
    });
  
}  