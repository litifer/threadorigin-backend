var request = require('request');
var requestify = require('requestify'); 
var constant = require('../config');

exports.createEmptyCart = function(req,res){
    requestify.request(constant.GUEST_CART,{
        method: 'POST',
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(function(response) {
            res.send({cart_id:response.body});
    }).catch(function(err){
        res.send(err);
    })
};

exports.addToCart = function(req,res){
    requestify.request(constant.GUEST_ADD_ITEM+req.params.id+'/items',{
    method: 'POST',
    headers:{
        'content-type' : 'application/json; charset=utf-8'
    },
    body : {
        "cart_item": {
            "quote_id": req.params.id,
            "product_type" : "configurable",
            "sku": req.body.sku,
            "qty": parseInt(req.body.qty),
            "product_option": {
                "extension_attributes": {
                "configurable_item_options" : [
                    {
                        "option_id": parseInt(req.body.color_id), 
                        "option_value": parseInt(req.body.color_value)
                    },
                    {
                        "option_id": parseInt(req.body.size_id), 
                        "option_value": parseInt(req.body.size_value)
                    }
                    ]
                  }
                }
        }
    }
    }).then(function(response) {
                res.json({message : 'Item added!'});                            
            })
    .catch(function(err){
        res.json({message : 'Unexpected error!'});
    });
};

exports.getCart = async function(req,res){
    
    try{
        let response  = await requestify.request(constant.GUEST_ADD_ITEM+req.params.id+'/items',{
                    method: 'GET',
                    headers:{
                        'content-type' : 'application/json; charset=utf-8'
                    }
                    });
    
        let result = await getItemInfo(response,req.params.id);                
        res.send(result);            

    }catch(err){
        console.log(err);
    }             

};

async function getItemInfo(response,cart){
    var data_products = await JSON.parse(response.body);
    var result = {};
    result['Product_Info'] = [];   
    var total = {};
    total['grand_total'] = await getTotal(cart)   
    for(var a=0; a < data_products.length;a++){   
        var item = {};
        item['name'] = await data_products[a].name;
        item['qty'] = await data_products[a].qty;
        item['item_id'] = await data_products[a].item_id;
        item['price'] = await total['grand_total'].items[a].base_row_total;
        item['product_option'] = await data_products[a].product_option;
        await result['Product_Info'].push(item);     
    } 
    result['Product_Info'].push(total); 
    return result;
}  

async function getTotal(cart){

    let response = await requestify.request(constant.GUEST_CART_TOTAL+cart+constant.GUEST_CART_TOTAL2,
    {
        method : 'GET',
        headers:{
            'content-type' : 'application/json; charset=utf-8'
        }
    }
    );

    let total = JSON.parse(response.body);
    return total;

}

exports.updateCartItem = async function(req,res){
    
    await requestify.request(constant.GUEST_ADD_ITEM+req.body.cart+'/items/'+req.body.item,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8'
            },
            body : {
                "cart_item": {
                    "quote_id": req.body.cart,
                    "itemId": req.body.item,
                    "qty": req.body.qty
                    }
            }
            });

    try{
        let response  = await requestify.request(constant.GUEST_ADD_ITEM+req.body.cart+'/items',{
                    method: 'GET',
                    headers:{
                        'content-type' : 'application/json; charset=utf-8'
                    }
                    });
    
        let result = await getItemInfo(response,req.body.cart);                
        res.send(result);            

    }
    catch(err){
        console.log(err);
    }                

}; 

exports.deleteCartItem = async function(req,res){
   await requestify.request(constant.GUEST_ADD_ITEM+req.body.cart+'/items/'+req.body.item,{
    method: 'DELETE',
    headers:{
        'content-type' : 'application/json; charset=utf-8'
    },
    body : {
        "cart_item": {
            "quote_id": req.body.cart,
            "itemId": req.body.item
            }
    }
    });

    try{
        let response  = await requestify.request(constant.GUEST_ADD_ITEM+req.body.cart+'/items',{
                    method: 'GET',
                    headers:{
                        'content-type' : 'application/json; charset=utf-8'
                    }
                    });
    
        let result = await getItemInfo(response,req.body.cart);                
        res.send(result);            

    }
    catch(err){
        console.log(err);
    }    

}; 

exports.mergeCart2 = function(req,res){

if(req.headers.authorization != null){        
    var token = 'Bearer '+req.headers.authorization;
    requestify.request(constant.GUEST_ADD_ITEM+req.body.cart,{
        method: 'PUT',
        headers:{
            'content-type' : 'application/json; charset=utf-8',
            'authorization' : token 
        },
        body : {            
            "customerId": req.body.customer,
            "storeId": 1              
        }
        }).then(function(response) {
                   res.send(response.body);                            
                })
        .catch(function(err){
            res.send(err.body);
        });
    }
    else
        res.send({error : "requires user token"});
}

exports.mergeCart = async function(req,res){

    if(req.body.Authorization != null){

        var token = 'Bearer '+req.body.Authorization;

        let user_cart  = await requestify.request(constant.USER_CART,{
                        method: 'POST',
                        headers:{
                            'content-type' : 'application/json; charset=utf-8',
                            'authorization' : token
                        }
                    });

        let response  = await requestify.request(constant.GUEST_ADD_ITEM+req.body.guest+'/items',{
                        method: 'GET',
                        headers:{
                            'content-type' : 'application/json; charset=utf-8'
                            }
                        });
        
        var id = '';
        if(user_cart.body.id == null)
            id = await user_cart.body;
        else
            id = await user_cart.body.id; 
        let data = await JSON.parse(await response.body);

        for(var a=0; a<data.length; a++)
          await addUserItems(token,data[a].sku,data[a].qty,data[a].product_option.extension_attributes,id);
        
        await res.json({message : 'Cart merged!'});  
    }

    else
        res.send({error : "requires user token"});
};

async function addUserItems(token,sku,qty,option,id){

        await requestify.request(constant.ADD_TO_CART,{
            method: 'POST',
            headers:{
                'authorization' : token,
                'content-type' : 'application/json; charset=utf-8'
            },
            body : {
                "cart_item": {
                    "quote_id": parseInt(id.replace(/"/g,'')),
                    "product_type" : "configurable",
                    "sku": sku,
                    "qty": parseInt(qty),
                    "product_option": {
                        "extension_attributes": option
                    }
                }
            }
        });

}

async function countries(country){
    try{
    var countries_ = await JSON.parse(constant.COUNTRIES);  
    for(var a=0; a<countries_.length; a++){
            if(await countries_[a].name.toLowerCase() == country.toLowerCase()){
                return await countries_[a].alpha2Code;
            }
      }
    }
    catch(err){console.log(err);}

}

async function setAddresses(req,street1,street2){
    try{    
    var rough = await {
                
        "addressInformation": {

        "shipping_address": {
             "region": await req.body.shipping_address.city,
             "region_id": 0,
             "region_code": "0",
             "country_id": ""+await countries(await req.body.shipping_address.country),
             "street": await street1,
            "postcode":await req.body.shipping_address.postcode,
            "city":await req.body.shipping_address.city,
            "firstname": await req.body.shipping_address.firstname,
            "lastname":await req.body.shipping_address.lastname,
            "telephone": await req.body.shipping_address.telephone,
            "email" : await req.body.shipping_address.email
        },

        "billing_address": {
          "region":await req.body.billing_address.city,
          "region_id": 0,
          "region_code": "a",
          "country_id": ""+await countries(req.body.billing_address.country),
          "street": await street2,
          "postcode":await req.body.billing_address.postcode,
          "city": await req.body.billing_address.city,
          "firstname":await req.body.billing_address.firstname,
          "lastname":await req.body.billing_address.lastname,
          "telephone":await req.body.billing_address.telephone,
          "email" : await req.body.billing_address.email
        },
        "shipping_carrier_code": "flatrate",
        "shipping_method_code": "flatrate"
        }

      };   
     return await rough; 
    }catch(err){console.log(err);}
}

exports.shippingMethod = async function(req,res){
    try{ 
            var street1 = {};
            var street2 = {};
            street1['street'] = [];
            street2['street'] = [];
            await street1['street'].push(req.body.shipping_address.street);
            await street2['street'].push(await req.body.billing_address.street); 
            let result = await setAddresses(req,street1['street'],street2['street']);
            try{
            await requestify.request(constant.GUEST_SHIPPING_INFO+req.body.guest+'/shipping-information',{
                method: 'POST',
                headers:{
                    'content-type' : 'application/json; charset=utf-8'
                },
                body : await result
            });
            await res.json({message : "Shipping method saved"});
            }catch(err){console.log(err);}    
    } catch(err){res.send(err.body);}           
}

exports.paymentMethod = async function(req,res){
    try{ 
            var street = {};
            street['street'] = [];            
            await street['street'].push(req.body.billing_address.street);
            let result = await setPayment(req,street['street']);
            try{
            let response  = await requestify.request(constant.GUEST_SHIPPING_INFO+req.body.guest+'/payment-information',{
                method: 'POST',
                headers:{
                    'content-type' : 'application/json; charset=utf-8'
                },
                body : await result
            });
            await res.json({order_id : JSON.parse(response.body)});
            }catch(err){console.log(err);}    
    } catch(err){res.send(err.body);}      
}

async function setPayment(req,street){
    try{    
    var rough = await {

        "email" : await req.body.billing_address.email,

        "paymentMethod": {
            "method": "razorpay"
        },
                        
        "billing_address": {
          "region":await req.body.billing_address.city,
          "region_id": 0,
          "region_code": "a",
          "country_id": ""+await countries(req.body.billing_address.country),
          "street": await street,
          "postcode":await req.body.billing_address.postcode,
          "city": await req.body.billing_address.city,
          "firstname":await req.body.billing_address.firstname,
          "lastname":await req.body.billing_address.lastname,
          "telephone":await req.body.billing_address.telephone,
          "email" : await req.body.billing_address.email
        }
      };   
     return await rough; 
    }catch(err){console.log(err);}
}
