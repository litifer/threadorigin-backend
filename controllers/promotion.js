var request = require('request');
var requestify = require('requestify'); 
var constant = require('../config');

exports.addCouponToCart = async function(req,res){

    let url; 

    if(req.body.customerToken != null){

        let token = 'Bearer '+req.body.customerToken;
        url = constant.GUEST_USER_CART + req.body.cartId + constant.APPLY_COUPON_CODE + req.body.coupon;
         // request to apply coupon
    requestify.request(url,{
        method: 'PUT',
        headers:{
            'authorization' : token,
            'content-type' : 'application/json; charset=utf-8'
        }
    }).then(response => {
        res.send(response.body);
    }).catch(err => {
        console.log(err);
        res.send(err)
    }); 
    }

    else{

        url = constant.GUEST_USER_CART + req.body.cartId + constant.APPLY_COUPON_CODE + req.body.coupon;

        // request to apply coupon
        requestify.request(url,{
            method: 'PUT',
            headers:{
                'content-type' : 'application/json; charset=utf-8'
            }
        }).then(response => {
            res.send(response.body);
        }).catch(err => {
            console.log(err);
            res.send(err)
        }); 
    }
      
};

async function getTotal(token){
    try{
        let response = await requestify.request(constant.CART_TOTAL,
        {
            method : 'GET',
            headers:{
                'authorization' : token,
                'content-type' : 'application/json; charset=utf-8'
            }
        }
        );     
    let total = JSON.parse(response.body);
    return total;
}
catch(err) {console.log(err);} 
}

exports.removeCouponFromCart = async function(req,res){

    if(req.headers.authorization != null){

        var token = 'Bearer '+req.headers.authorization;
        var url = constant.ADD_COUPON_CODE;
        var result = {};
        result['remove_status'] = [];
        result['info'] = [];
        let response  = await requestify.request(url,{
                        method: 'DELETE',
                        headers:{
                            'authorization' : token,
                            'content-type' : 'application/json; charset=utf-8'
                        }
                        });
        await result['remove_status'].push(await response.body);
        await result['info'].push(await getTotal(token));
        await res.send(await result);              
    }
    
    else
        res.send({error : "requires user token"});

}

exports.generateCoupon = async (req, res) => {
     let response = await requestify.request(constant.GENERATE_COUPON_CODE, {
        "couponSpec": {
            "ruleId": "1",
            "format": "alphanum",
            "quantity": "1",
            "length": "10",
            "prefix": "THREAD ORIGIN-Jul-",
            "suffix": "",
            "extensionAttributes": {}
          }
    })

    return ({"coupon": response})
}